package ru.traf.test.helpers;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.Credit;
import ru.traf.creditcalculator.core.CreditType;
import ru.traf.creditcalculator.core.PlanPayment;
import ru.traf.creditcalculator.core.Rate;

public class CreditHelperForTests {
    private Credit credit;
    private Rate rate;
    private PlanPayment planPayment;

    public CreditHelperForTests() {
        Calendar planPaymentStartDate = new GregorianCalendar(2016, Calendar.JANUARY, 28);
        rate = new Rate(BigDecimal.valueOf(13.5), planPaymentStartDate);
        planPayment = new PlanPayment(BigDecimal.valueOf(15000), planPaymentStartDate);
        credit = new Credit("Test", CreditType.Annuity, BigDecimal.valueOf(50000), 60, rate, planPayment);
    }

    public Credit getCredit() {
        return credit;
    }

    public Rate getRate() {
        return rate;
    }

    public PlanPayment getPlanPayment() {
        return planPayment;
    }
}
