package ru.traf.test.helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ru.traf.creditcalculator.core.CreditEventType;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraph;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;

/**
 * Импорт/экспорт графика платежей из CSV
 */
public class PaymentGraphCsvHelper {
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
    private static DecimalFormat decimalFormatter = new DecimalFormat("0.00");
    private static String separator = ";";
    private static String csvFolder = "./src/test/res/creditCsv/";

    private PaymentGraphCsvHelper() {
    }

    static {
        decimalFormatter.setParseBigDecimal(true);
    }

    /**
     * Экспортировать график платежей в CSV
     */
    public static void exportToCsv(PaymentGraph paymentGraph, String fileName) throws IOException {
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFolder + fileName), "UTF-8"));
            writeCaption(writer);
            for (PaymentGraphEvent event : paymentGraph.getPaymentGraphEvents()) {
                writeEvent(event, writer);
            }
            writer.flush();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    private static void writeCaption(Writer writer) throws IOException {
        writeCsvString(new String[]{"№", "Дата", "Сумма платежа", "Сумма в погашение основного долга", "Сумма в погашение процентов",
                "Остаток задолженности", "Описание", "Тип события"}, writer);
    }

    private static void writeEvent(PaymentGraphEvent event, Writer writer) throws IOException {
        int paymentPeriodNumber = event.getPaymentPeriodNumber();
        Calendar eventDate = event.getDate();
        BigDecimal paymentSum = event.getPaymentSum();
        BigDecimal percentSum = event.getPercentSum();
        BigDecimal mainDebtSum = event.getMainDebtSum();
        BigDecimal remainMainDebtSum = event.getRemainMainDebtSum();
        CreditEventType eventType = event.getEventType();
        String description = event.getDescription();
        writeCsvString(new String[]{String.valueOf(paymentPeriodNumber), dateFormatter.format(eventDate.getTime()), paymentSum != null ? decimalFormatter.format(paymentSum) : "",
                mainDebtSum != null ? decimalFormatter.format(mainDebtSum) : "", percentSum != null ? decimalFormatter.format(percentSum) : "",
                remainMainDebtSum != null ? decimalFormatter.format(remainMainDebtSum) : "", description, eventType.toString()}, writer);
    }

    private static void writeCsvString(String[] values, Writer writer) throws IOException {
        for (int i = 0; i < values.length; i++) {
            writer.append(values[i]);
            if (i != values.length - 1) {
                writer.append(separator);
            }
        }
        writer.append('\n');
    }

    /**
     * Импортировать график платежей из CSV
     */
    public static PaymentGraph importFromCsv(String fileName) throws IOException, ParseException {
        BufferedReader reader = null;
        PaymentGraph paymentGraph = new PaymentGraph();
        try {
            reader = new BufferedReader(new FileReader(csvFolder + fileName));
            reader.readLine(); // пропускаем заголовок
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(separator);
                paymentGraph.AddPaymentGraphEvent(readEventFromCsvString(values));
            }
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return paymentGraph;
    }

    private static PaymentGraphEvent readEventFromCsvString(String[] values) throws ParseException {
        int paymentPeriodNumber = Integer.parseInt(values[0]);
        Calendar eventDate = Calendar.getInstance();
        eventDate.setTime(dateFormatter.parse(values[1]));
        BigDecimal paymentSum = null;
        BigDecimal percentSum = null;
        BigDecimal mainDebtSum = null;
        BigDecimal remainMainDebtSum = null;
        if (!values[2].equals("")) {
            paymentSum = (BigDecimal) decimalFormatter.parse(values[2]);
            mainDebtSum = (BigDecimal) decimalFormatter.parse(values[3]);
            percentSum = (BigDecimal) decimalFormatter.parse(values[4]);
            remainMainDebtSum = (BigDecimal) decimalFormatter.parse(values[5]);
        }
        String description = values[6];
        CreditEventType eventType = CreditEventType.valueOf(values[7]);
        return new PaymentGraphEvent(paymentPeriodNumber, eventDate, paymentSum, percentSum, mainDebtSum, remainMainDebtSum, eventType, description);
    }
}
