package ru.traf.creditcalculator.core.creditevents;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.CreditContext;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.PlanPayment;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.percentsumcalculators.IPercentSumCalculator;
import ru.traf.test.helpers.CreditHelperForTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PlanPaymentEventTest {
    private CreditHelperForTests creditHelper;
    private ICreditContext context;
    private BigDecimal mockedPercentSum;
    private IPercentSumCalculator mockedPercentSumCalculator;

    @Before
    public void initialize() {
        creditHelper = new CreditHelperForTests();
        context = new CreditContext(creditHelper.getCredit());
        mockedPercentSum = BigDecimal.valueOf(8000);
        mockedPercentSumCalculator = mock(IPercentSumCalculator.class);
        when(mockedPercentSumCalculator.calculate(notNull(Calendar.class), notNull(Calendar.class), notNull(BigDecimal.class), notNull(BigDecimal.class))).thenReturn(mockedPercentSum);
    }

    @Test
    public void testHandle() throws CreditCalculationException {
        context.goToNextPaymentPeriod();
        PlanPaymentEvent creditEvent = new PlanPaymentEvent(context.getNextPlanPaymentDate(), creditHelper.getPlanPayment().getStartDate(), mockedPercentSumCalculator);
        PaymentGraphEvent paymentGraphEvent = creditEvent.handle(context);

        assertEquals(1, paymentGraphEvent.getPaymentPeriodNumber());
        assertSame(context.getNextPlanPaymentDate(), paymentGraphEvent.getDate());

        BigDecimal paymentSum = creditHelper.getPlanPayment().getSum();
        assertSame(paymentSum, paymentGraphEvent.getPaymentSum());
        assertEquals(mockedPercentSum, paymentGraphEvent.getPercentSum());

        BigDecimal mainDebtSum = paymentSum.subtract(mockedPercentSum);
        assertEquals(mainDebtSum, paymentGraphEvent.getMainDebtSum());

        BigDecimal remainMainDebtSum = creditHelper.getCredit().getSum().subtract(mainDebtSum);
        assertEquals(remainMainDebtSum, paymentGraphEvent.getRemainMainDebtSum());

        assertEquals("Плановый платёж", paymentGraphEvent.getDescription());
        assertEquals(creditHelper.getCredit().getPeriodInMonth() - 1, context.getRemainPeriodInMonth());
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testHandleIfPlanPaymentNotCoverPercent() throws CreditCalculationException {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        creditHelper.getPlanPayment().setSum(BigDecimal.valueOf(1000));

        expectedException.expect(CreditCalculationException.class);
        expectedException.expectMessage(String.format("Сумма планового платежа %.2f меньше или равна сумме процентов %.2f, платёж от %s",
                creditHelper.getPlanPayment().getSum(), mockedPercentSum, dateFormatter.format(creditHelper.getPlanPayment().getStartDate().getTime())));

        context.goToNextPaymentPeriod();
        PlanPaymentEvent creditEvent = new PlanPaymentEvent(context.getNextPlanPaymentDate(), creditHelper.getPlanPayment().getStartDate(), mockedPercentSumCalculator);
        creditEvent.handle(context);
    }

    @Test
    public void testHandleIfPlanPaymentOnlyPercent() throws CreditCalculationException {
        context.goToNextPaymentPeriod();
        PlanPaymentEvent creditEvent = new PlanPaymentEvent(context.getNextPlanPaymentDate(), creditHelper.getPlanPayment().getStartDate(), mockedPercentSumCalculator);
        creditEvent.setOnlyPercent(true);
        PaymentGraphEvent paymentGraphEvent = creditEvent.handle(context);

        assertEquals(1, paymentGraphEvent.getPaymentPeriodNumber());
        assertSame(context.getNextPlanPaymentDate(), paymentGraphEvent.getDate());

        assertEquals(mockedPercentSum, paymentGraphEvent.getPercentSum());
        assertEquals(mockedPercentSum, paymentGraphEvent.getPaymentSum());

        BigDecimal mainDebtSum = BigDecimal.ZERO;
        assertEquals(mainDebtSum, paymentGraphEvent.getMainDebtSum());

        assertEquals(creditHelper.getCredit().getSum(), paymentGraphEvent.getRemainMainDebtSum());

        assertEquals("Плановый платёж", paymentGraphEvent.getDescription());
        assertEquals(creditHelper.getCredit().getPeriodInMonth() - 1, context.getRemainPeriodInMonth());
    }

    @Test
    public void testRemainPeriodAreNotChangedIfFirstPlanPaymentOnlyPercent() throws CreditCalculationException {
        creditHelper.getCredit().setFirstPaymentOnlyPercent(true);
        context.goToNextPaymentPeriod();
        PlanPaymentEvent creditEvent = new PlanPaymentEvent(context.getNextPlanPaymentDate(), creditHelper.getPlanPayment().getStartDate(), mockedPercentSumCalculator);
        creditEvent.setOnlyPercent(true);
        creditEvent.handle(context);
        assertEquals(creditHelper.getCredit().getPeriodInMonth(), context.getRemainPeriodInMonth());
    }

    @Test
    public void testAdvancePaymentsWithDelayedRecalculationCall() throws CreditCalculationException {
        context.goToNextPaymentPeriod();
        AdvancePaymentEvent mockedAdvancePaymentEventOne = mock(AdvancePaymentEvent.class);
        mockedAdvancePaymentEventOne.setIsDelayedRecalculation(true);
        AdvancePaymentEvent mockedAdvancePaymentEventTwo = mock(AdvancePaymentEvent.class);
        mockedAdvancePaymentEventTwo.setIsDelayedRecalculation(true);
        PlanPaymentEvent creditEvent = new PlanPaymentEvent(context.getNextPlanPaymentDate(), creditHelper.getPlanPayment().getStartDate(), mockedPercentSumCalculator);
        creditEvent.setAdvancePaymentEventsWithDelayedRecalculation(Arrays.asList(mockedAdvancePaymentEventOne, mockedAdvancePaymentEventTwo));
        creditEvent.handle(context);
        verify(mockedAdvancePaymentEventOne).handleAdvancePaymentByType(context);
        verify(mockedAdvancePaymentEventTwo).handleAdvancePaymentByType(context);
    }
}
