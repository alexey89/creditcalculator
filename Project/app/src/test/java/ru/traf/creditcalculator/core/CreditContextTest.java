package ru.traf.creditcalculator.core;

import com.annimon.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.common.SpecialDay;
import ru.traf.test.helpers.CreditHelperForTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class CreditContextTest {
    private CreditHelperForTests creditHelper;
    private ICreditContext context;

    @Before
    public void initialize() {
        creditHelper = new CreditHelperForTests();
        context = new CreditContext(creditHelper.getCredit());
    }

    @Test
    public void testCreditContextInitialization() {
        assertEquals(0, context.getCurrentPeriodNumber());
        assertEquals(60, context.getRemainPeriodInMonth());
        assertSame(creditHelper.getRate(), context.getCurrentRate());
        assertSame(creditHelper.getPlanPayment(), context.getCurrentPlanPayment());
        assertEquals(50000, context.getRemainMainDebtSum().intValue());
        assertNull(context.getPreviousPlanPaymentDate());
        assertNull(context.getNextPlanPaymentDate());
        assertFalse(context.isFirstPlanPayment());
        assertNull(context.getCurrentPaymentPeriodPlanPaymentChanges());
        assertNull(context.getCurrentPaymentPeriodRateChanges());
        assertNull(context.getCurrentPaymentPeriodAdvancePayments());
    }

    @Test
    public void testGoToNextPaymentPeriod() {
        context.goToNextPaymentPeriod();
        assertEquals(1, context.getCurrentPeriodNumber());
        assertEquals(60, context.getRemainPeriodInMonth());
        assertSame(creditHelper.getRate(), context.getCurrentRate());
        assertSame(creditHelper.getPlanPayment(), context.getCurrentPlanPayment());
        assertEquals(50000, context.getRemainMainDebtSum().intValue());
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2015, Calendar.DECEMBER, 28));
        assertEquals(creditHelper.getPlanPayment().getStartDate(), context.getNextPlanPaymentDate());
        assertTrue(context.isFirstPlanPayment());
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).count());
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodRateChanges()).count());
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).count());

        context.goToNextPaymentPeriod();
        assertEquals(2, context.getCurrentPeriodNumber());
        assertEquals(60, context.getRemainPeriodInMonth());
        assertSame(creditHelper.getRate(), context.getCurrentRate());
        assertSame(creditHelper.getPlanPayment(), context.getCurrentPlanPayment());
        assertEquals(50000, context.getRemainMainDebtSum().intValue());
        assertEquals(creditHelper.getPlanPayment().getStartDate(), context.getPreviousPlanPaymentDate());
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 28));
        assertFalse(context.isFirstPlanPayment());
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).count());
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodRateChanges()).count());
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).count());
    }

    @Test
    public void testGoToNextPaymentPeriodIfFirstPaymentOnlyPercent() {
        creditHelper.getCredit().setFirstPaymentOnlyPercent(true);
        creditHelper.getCredit().setCreditDate(new GregorianCalendar(2016, Calendar.JANUARY, 12));

        context.goToNextPaymentPeriod();
        assertSame(creditHelper.getCredit().getCreditDate(), context.getPreviousPlanPaymentDate());
        assertEquals(creditHelper.getPlanPayment().getStartDate(), context.getNextPlanPaymentDate());

        context.goToNextPaymentPeriod();
        assertEquals(creditHelper.getPlanPayment().getStartDate(), context.getPreviousPlanPaymentDate());
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 28));
    }

    @Test
    public void testGoToNextPaymentPeriodIfPaymentOnLastMonthDay() {
        creditHelper.getPlanPayment().setIsLastDayOfMonth(true);

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2015, Calendar.DECEMBER, 31));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.JANUARY, 31));

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2016, Calendar.JANUARY, 31));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 29));

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 29));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.MARCH, 31));
    }

    @Test
    public void testGoToNextPaymentPeriodIfTransferDaysOff() {
        creditHelper.getPlanPayment().setTransferDaysOff(true);

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2015, Calendar.DECEMBER, 28));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.JANUARY, 28));

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2016, Calendar.JANUARY, 28));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 29));

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 29));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.MARCH, 28));
    }

    @Test
    public void testGoToNextPaymentPeriodIfPaymentOnLastMonthDayAndTransferDaysOff() {
        creditHelper.getPlanPayment().setIsLastDayOfMonth(true);
        creditHelper.getPlanPayment().setTransferDaysOff(true);
        // Для проверки того, что фиктивная предыдущая дата первого планового платежа не переносится с выходных дней
        creditHelper.getCredit().addDayOff(new SpecialDay(new GregorianCalendar(2015, Calendar.DECEMBER, 31)));

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2015, Calendar.DECEMBER, 31));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 1));

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 1));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 29));

        context.goToNextPaymentPeriod();
        assertEquals(context.getPreviousPlanPaymentDate(), new GregorianCalendar(2016, Calendar.FEBRUARY, 29));
        assertEquals(context.getNextPlanPaymentDate(), new GregorianCalendar(2016, Calendar.MARCH, 31));
    }

    @Test
    public void testGetCurrentPaymentPeriodEvents() {
        Credit credit = creditHelper.getCredit();
        credit.addPlanPayment(new PlanPayment(BigDecimal.valueOf(12000), new GregorianCalendar(2016, Calendar.FEBRUARY, 15)));
        credit.addPlanPayment(new PlanPayment(BigDecimal.valueOf(14000), new GregorianCalendar(2016, Calendar.JANUARY, 28)));
        credit.addPlanPayment(new PlanPayment(BigDecimal.valueOf(13000), new GregorianCalendar(2016, Calendar.FEBRUARY, 5)));

        credit.addRate(new Rate(BigDecimal.valueOf(13), new GregorianCalendar(2016, Calendar.FEBRUARY, 28)));
        credit.addRate(new Rate(BigDecimal.valueOf(13.2), new GregorianCalendar(2016, Calendar.FEBRUARY, 5)));
        credit.addRate(new Rate(BigDecimal.valueOf(13.4), new GregorianCalendar(2016, Calendar.JANUARY, 7)));

        credit.addAdvancePayment(new AdvancePayment(BigDecimal.valueOf(40000), new GregorianCalendar(2016, Calendar.JANUARY, 5), AdvancePaymentType.PeriodDecreasing));
        credit.addAdvancePayment(new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2015, Calendar.DECEMBER, 27), AdvancePaymentType.PaymentDecreasing));
        credit.addAdvancePayment(new AdvancePayment(BigDecimal.valueOf(55000), new GregorianCalendar(2016, Calendar.MARCH, 15), AdvancePaymentType.PeriodDecreasing));
        credit.addAdvancePayment(new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2016, Calendar.FEBRUARY, 29), AdvancePaymentType.PeriodDecreasing));
        credit.addAdvancePayment(new AdvancePayment(BigDecimal.valueOf(35000), new GregorianCalendar(2015, Calendar.DECEMBER, 28), AdvancePaymentType.PaymentDecreasing));
        credit.addAdvancePayment(new AdvancePayment(BigDecimal.valueOf(45000), new GregorianCalendar(2016, Calendar.FEBRUARY, 28), AdvancePaymentType.PeriodDecreasing));

        context = new CreditContext(credit);

        context.goToNextPaymentPeriod();
        assertEquals(1, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).count());
        assertEquals(14000, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).findFirst().get().getSum().intValue());

        assertEquals(1, Stream.of(context.getCurrentPaymentPeriodRateChanges()).count());
        assertEquals(BigDecimal.valueOf(13.5), Stream.of(context.getCurrentPaymentPeriodRateChanges()).findFirst().get().getYearRateInPercent());
        assertEquals(Stream.of(context.getCurrentPaymentPeriodRateChanges()).findFirst().get().getStartDate(), new GregorianCalendar(2016, Calendar.JANUARY, 28));

        assertEquals(2, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).count());
        assertEquals(35000, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).findFirst().get().getSum().intValue());
        assertEquals(40000, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).skip(1).findFirst().get().getSum().intValue());

        context.goToNextPaymentPeriod();
        assertEquals(2, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).count());
        assertEquals(13000, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).findFirst().get().getSum().intValue());
        assertEquals(12000, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).skip(1).findFirst().get().getSum().intValue());

        assertEquals(2, Stream.of(context.getCurrentPaymentPeriodRateChanges()).count());
        assertEquals(BigDecimal.valueOf(13.2), Stream.of(context.getCurrentPaymentPeriodRateChanges()).findFirst().get().getYearRateInPercent());
        assertEquals(BigDecimal.valueOf(13), Stream.of(context.getCurrentPaymentPeriodRateChanges()).skip(1).findFirst().get().getYearRateInPercent());

        assertEquals(1, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).count());
        assertEquals(45000, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).findFirst().get().getSum().intValue());

        context.goToNextPaymentPeriod();
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodPlanPaymentChanges()).count());
        assertEquals(0, Stream.of(context.getCurrentPaymentPeriodRateChanges()).count());

        assertEquals(2, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).count());
        assertEquals(50000, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).findFirst().get().getSum().intValue());
        assertEquals(55000, Stream.of(context.getCurrentPaymentPeriodAdvancePayments()).skip(1).findFirst().get().getSum().intValue());
    }
}
