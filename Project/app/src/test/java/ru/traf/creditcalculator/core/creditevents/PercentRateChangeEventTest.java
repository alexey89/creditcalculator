package ru.traf.creditcalculator.core.creditevents;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.CreditContext;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.Rate;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;
import ru.traf.test.helpers.CreditHelperForTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PercentRateChangeEventTest {
    private static DecimalFormat decimalFormatter = new DecimalFormat("#.##");
    private CreditHelperForTests creditHelper;
    private ICreditContext context;
    private BigDecimal changedPlanPaymentSum;
    private Rate newRate;
    private Calendar creditEventDate;
    private PercentRateChangeEvent creditEvent;
    private IPlanPaymentCalculator mockedPlanPaymentCalculator;

    @Before
    public void initialize() {
        creditHelper = new CreditHelperForTests();

        changedPlanPaymentSum = BigDecimal.valueOf(12000);
        mockedPlanPaymentCalculator = mock(IPlanPaymentCalculator.class);
        when(mockedPlanPaymentCalculator.calculate(notNull(BigDecimal.class), anyInt(), anyDouble())).thenReturn(changedPlanPaymentSum);

        newRate = new Rate(BigDecimal.valueOf(12), Calendar.getInstance());
        creditEventDate = new GregorianCalendar(2016, Calendar.JANUARY, 31);
        creditEvent = new PercentRateChangeEvent(creditEventDate, newRate, mockedPlanPaymentCalculator);

        context = new CreditContext(creditHelper.getCredit());
        context.goToNextPaymentPeriod();
    }

    @Test
    public void testHandle() {
        assertSame(creditHelper.getRate(), context.getCurrentRate());

        PaymentGraphEvent paymentGraphEvent = creditEvent.handle(context);
        assertSame(newRate, context.getCurrentRate());
        assertSame(creditEventDate, paymentGraphEvent.getDate());
        assertEquals(1, paymentGraphEvent.getPaymentPeriodNumber());
        String.format("Изменение процентной ставки с %s на %s", decimalFormatter.format(creditHelper.getRate().getYearRateInPercent()), decimalFormatter.format(newRate.getYearRateInPercent()));
        assertEquals(String.format("Изменение процентной ставки с %s%% на %s%%",
                        decimalFormatter.format(creditHelper.getRate().getYearRateInPercent()), decimalFormatter.format(newRate.getYearRateInPercent())),
                paymentGraphEvent.getDescription());
    }

    @Test
    public void testHandleWithPlanPaymentRecalculation() {
        context.decreaseRemainMainDebtSum(BigDecimal.valueOf(10000));
        creditEvent.handle(context);
        assertNotSame(context.getCredit().getSum(), context.getRemainMainDebtSum());
        verify(mockedPlanPaymentCalculator).calculate(context.getRemainMainDebtSum(), context.getRemainPeriodInMonth(), context.getCurrentRate().getMonthRate());
        assertEquals(context.getCurrentPlanPayment().getSum(), changedPlanPaymentSum);
    }

    @Test
    public void testHandleWithoutPlanPaymentRecalculation() {
        newRate.setRecalculatePlanPayment(false);
        context.decreaseRemainMainDebtSum(BigDecimal.valueOf(10000));
        creditEvent.handle(context);
        assertNotSame(context.getCredit().getSum(), context.getRemainMainDebtSum());
        verify(mockedPlanPaymentCalculator, never()).calculate(context.getCredit().getSum(), context.getRemainPeriodInMonth(), context.getCurrentRate().getMonthRate());
        assertSame(context.getCurrentPlanPayment(), creditHelper.getPlanPayment());
    }
}
