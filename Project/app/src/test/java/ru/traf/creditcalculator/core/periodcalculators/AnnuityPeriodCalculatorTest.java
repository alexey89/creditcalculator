package ru.traf.creditcalculator.core.periodcalculators;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;

import ru.traf.creditcalculator.core.Rate;

import static org.junit.Assert.assertEquals;

public class AnnuityPeriodCalculatorTest {
    @Test
    public void testCalculate() {
        IPeriodCalculator calculator = new AnnuityPeriodCalculator();
        BigDecimal remainMainDebtSum = BigDecimal.valueOf(631206.27);
        Rate percentRate = new Rate(BigDecimal.valueOf(12), Calendar.getInstance());
        BigDecimal planPaymentSum = BigDecimal.valueOf(22244.45);

        assertEquals(34, calculator.calculate(remainMainDebtSum, planPaymentSum, percentRate.getMonthRate()));
    }
}
