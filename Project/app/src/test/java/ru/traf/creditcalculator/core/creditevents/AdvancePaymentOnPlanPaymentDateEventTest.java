package ru.traf.creditcalculator.core.creditevents;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.AdvancePayment;
import ru.traf.creditcalculator.core.AdvancePaymentType;
import ru.traf.creditcalculator.core.CreditContext;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.periodcalculators.IPeriodCalculator;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;
import ru.traf.test.helpers.CreditHelperForTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AdvancePaymentOnPlanPaymentDateEventTest {
    private final String calculationExceptionMessage = "Сумма досрочного платежа %.2f меньше или равна нулю после вычета планового платежа, равного %.2f";
    private CreditHelperForTests creditHelper;
    private AdvancePayment payment;
    private AdvancePaymentEvent creditEvent;
    private ICreditContext context;
    private BigDecimal changedPlanPaymentSum;
    private int changedPeriod;

    @Before
    public void initialize() {
        creditHelper = new CreditHelperForTests();

        Calendar paymentDate = new GregorianCalendar(2016, Calendar.FEBRUARY, 15);
        BigDecimal paymentSum = BigDecimal.valueOf(30000);
        payment = new AdvancePayment(paymentSum, paymentDate, AdvancePaymentType.PaymentDecreasing);
        payment.setPayOnPlanPaymentDate(true);

        changedPeriod = 55;
        IPeriodCalculator mockedPeriodCalculator = mock(IPeriodCalculator.class);
        when(mockedPeriodCalculator.calculate(notNull(BigDecimal.class), notNull(BigDecimal.class), anyDouble())).thenReturn(changedPeriod);

        changedPlanPaymentSum = BigDecimal.valueOf(12000);
        IPlanPaymentCalculator mockedPlanPaymentCalculator = mock(IPlanPaymentCalculator.class);
        when(mockedPlanPaymentCalculator.calculate(notNull(BigDecimal.class), anyInt(), anyDouble())).thenReturn(changedPlanPaymentSum);

        context = new CreditContext(creditHelper.getCredit());
        context.goToNextPaymentPeriod();

        creditEvent = new AdvancePaymentOnPlanPaymentDateEvent(context.getNextPlanPaymentDate(), payment, mockedPeriodCalculator, mockedPlanPaymentCalculator);
    }

    @Test
    public void testHandleIfNotSubtractPlanPaymentSum() throws CreditCalculationException {
        PaymentGraphEvent paymentGraphEvent = creditEvent.handle(context);

        assertEquals(1, paymentGraphEvent.getPaymentPeriodNumber());
        assertSame(context.getNextPlanPaymentDate(), paymentGraphEvent.getDate());
        assertSame(payment.getSum(), paymentGraphEvent.getPaymentSum());
        assertEquals(BigDecimal.ZERO, paymentGraphEvent.getPercentSum());
        assertEquals(payment.getSum(), paymentGraphEvent.getMainDebtSum());

        BigDecimal remainMainDebtSum = creditHelper.getCredit().getSum().subtract(payment.getSum());
        assertEquals(remainMainDebtSum, paymentGraphEvent.getRemainMainDebtSum());
        assertEquals("Досрочный платёж", paymentGraphEvent.getDescription());
    }

    @Test
    public void testHandleIfSubtractPlanPaymentSum() throws CreditCalculationException {
        payment.setSubtractPlanPaymentSum(true);
        PaymentGraphEvent paymentGraphEvent = creditEvent.handle(context);

        BigDecimal paymentSum = payment.getSum().subtract(creditHelper.getPlanPayment().getSum());
        assertEquals(paymentSum, paymentGraphEvent.getPaymentSum());
        assertEquals(BigDecimal.ZERO, paymentGraphEvent.getPercentSum());
        assertEquals(paymentSum, paymentGraphEvent.getMainDebtSum());

        BigDecimal remainMainDebtSum = creditHelper.getCredit().getSum().subtract(paymentSum);
        assertEquals(remainMainDebtSum, paymentGraphEvent.getRemainMainDebtSum());
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testHandleIfPaymentSumEqualsZeroAfterSubtractPlanPaymentSum() throws CreditCalculationException {
        expectedException.expect(CreditCalculationException.class);
        expectedException.expectMessage(String.format(calculationExceptionMessage, creditHelper.getPlanPayment().getSum(), creditHelper.getPlanPayment().getSum()));
        payment.setSubtractPlanPaymentSum(true);
        payment.setSum(creditHelper.getPlanPayment().getSum());
        creditEvent.handle(context);
    }

    @Test
    public void testHandleIfPaymentSumLessThanZeroAfterSubtractPlanPaymentSum() throws CreditCalculationException {
        expectedException.expect(CreditCalculationException.class);
        expectedException.expectMessage(String.format(calculationExceptionMessage, BigDecimal.TEN, creditHelper.getPlanPayment().getSum()));
        payment.setSubtractPlanPaymentSum(true);
        payment.setSum(BigDecimal.TEN);
        creditEvent.handle(context);
    }

    @Test
    public void testHandleWithPaymentDecreasing() throws CreditCalculationException {
        creditEvent.handle(context);
        assertEquals(creditHelper.getCredit().getPeriodInMonth(), context.getRemainPeriodInMonth());
        assertNotSame(changedPlanPaymentSum, context.getCurrentPlanPayment());
        assertEquals(changedPlanPaymentSum, context.getCurrentPlanPayment().getSum());
    }

    @Test
    public void testHandleWithPeriodDecreasing() throws CreditCalculationException {
        payment.setType(AdvancePaymentType.PeriodDecreasing);
        creditEvent.handle(context);
        assertSame(creditHelper.getPlanPayment(), context.getCurrentPlanPayment());
        assertEquals(changedPeriod, context.getRemainPeriodInMonth());
    }
}
