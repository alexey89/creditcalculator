package ru.traf.creditcalculator.core.planpaymentcalculators;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

import ru.traf.creditcalculator.core.Rate;
import ru.traf.creditcalculator.core.common.helpers.MathHelper;

import static org.junit.Assert.assertEquals;

public class AnnuityPlanPaymentCalculatorTest {
    private IPlanPaymentCalculator calculator;
    private BigDecimal creditSum;
    private int creditPeriodInMonth;
    private Rate percentRate;

    @Before
    public void initialize() {
        calculator = new AnnuityPlanPaymentCalculator();
        creditSum = BigDecimal.valueOf(1121000);
        percentRate = new Rate(BigDecimal.valueOf(13.5), Calendar.getInstance());
        creditPeriodInMonth = 180;
    }

    @Test
    public void testCalculate() {
        assertEquals(BigDecimal.valueOf(14554.15), calculator.calculate(creditSum, creditPeriodInMonth, percentRate.getMonthRate())
                .setScale(2, RoundingMode.HALF_UP));
    }
}
