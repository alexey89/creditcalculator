package ru.traf.creditcalculator.core.common.helpers;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.common.SpecialDay;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SpecialDayHelperTest {
    @Test
    public void testIsSpecialDay() {
        Collection<SpecialDay> daysOff = SpecialDayHelper.getStandardDaysOff();
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.JANUARY, 17), daysOff)); // 17 января 2016 (воскресенье)
        assertFalse(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.FEBRUARY, 29), daysOff)); // 29 февраля 2016 (понедельник)
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.JANUARY, 1), daysOff)); // 1 января 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.JANUARY, 2), daysOff)); // 2 января 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.FEBRUARY, 23), daysOff)); // 23 февраля 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.MARCH, 8), daysOff)); // 8 марта 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.MAY, 1), daysOff)); // 1 мая 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.MAY, 9), daysOff)); // 9 мая 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.JUNE, 12), daysOff)); // 12 июня 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.NOVEMBER, 4), daysOff)); // 4 ноября 2016
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2017, Calendar.MARCH, 8), daysOff)); // 8 марта 2017
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2020, Calendar.MAY, 10), daysOff)); // 4 ноября 2020

        daysOff.add(new SpecialDay((byte) Calendar.MONDAY));
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.FEBRUARY, 29), daysOff)); // 29 февраля 2016 (понедельник)

        daysOff.add(new SpecialDay(new GregorianCalendar(2016, 0, 19))); // 19 января 2016 (вторник)
        assertTrue(SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.JANUARY, 19), daysOff));
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionIfDateParameterIsNull() {
        SpecialDayHelper.isSpecialDay(null, new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionIfSpecialDayParameterIsNull() {
        SpecialDayHelper.isSpecialDay(new GregorianCalendar(2016, Calendar.JANUARY, 19), null);
    }
}