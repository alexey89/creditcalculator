package ru.traf.creditcalculator.core.percentsumcalculators;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.Rate;
import ru.traf.creditcalculator.core.common.helpers.MathHelper;

import static org.junit.Assert.assertEquals;

public class StandardPercentSumCalculatorTest {
    private IPercentSumCalculator calculator;
    private BigDecimal remainManiDebtSum;
    private Rate percentRate;

    @Before
    public void initialize() {
        calculator = new StandardPercentSumCalculator();
        remainManiDebtSum = BigDecimal.valueOf(475000);
        percentRate = new Rate(BigDecimal.valueOf(13.5), Calendar.getInstance());
    }

    @Test
    public void testCalculate() {
        Calendar startDate = new GregorianCalendar(2016, Calendar.JANUARY, 20);
        Calendar endDate = new GregorianCalendar(2016, Calendar.FEBRUARY, 20);
        assertEquals(BigDecimal.valueOf(5431.35), getRoundPercentSum(startDate, endDate));

        endDate = startDate;
        startDate = new GregorianCalendar(2015, Calendar.DECEMBER, 20);
        assertEquals(BigDecimal.valueOf(5436.63), getRoundPercentSum(startDate, endDate));

        startDate = new GregorianCalendar(2014, Calendar.AUGUST, 5);
        assertEquals(BigDecimal.valueOf(93630.47), getRoundPercentSum(startDate, endDate));
    }

    private BigDecimal getRoundPercentSum(Calendar startDate, Calendar endDate) {
        return calculator.calculate(startDate, endDate, remainManiDebtSum, percentRate.getYearRate())
                .setScale(2, RoundingMode.HALF_UP);
    }
}
