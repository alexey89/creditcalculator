package ru.traf.creditcalculator.core.creditevents;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.verification.VerificationMode;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.AdvancePayment;
import ru.traf.creditcalculator.core.AdvancePaymentType;
import ru.traf.creditcalculator.core.CreditContext;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.percentsumcalculators.IPercentSumCalculator;
import ru.traf.creditcalculator.core.periodcalculators.IPeriodCalculator;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;
import ru.traf.test.helpers.CreditHelperForTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.anyDouble;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.notNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AdvancePaymentEventTest {
    private CreditHelperForTests creditHelper;
    private AdvancePayment payment;
    private AdvancePaymentEvent creditEvent;
    private ICreditContext context;
    private BigDecimal changedPlanPaymentSum;
    private IPeriodCalculator mockedPeriodCalculator;
    private IPlanPaymentCalculator mockedPlanPaymentCalculator;
    private BigDecimal mockedPercentSum;
    private int changedPeriod;

    @Before
    public void initialize() {
        creditHelper = new CreditHelperForTests();

        Calendar paymentDate = new GregorianCalendar(2016, Calendar.FEBRUARY, 15);
        BigDecimal paymentSum = BigDecimal.valueOf(30000);

        mockedPercentSum = BigDecimal.valueOf(8000);
        IPercentSumCalculator mockedPercentSumCalculator = mock(IPercentSumCalculator.class);
        when(mockedPercentSumCalculator.calculate(notNull(Calendar.class), notNull(Calendar.class), notNull(BigDecimal.class), notNull(BigDecimal.class))).thenReturn(mockedPercentSum);

        changedPeriod = 55;
        mockedPeriodCalculator = mock(IPeriodCalculator.class);
        when(mockedPeriodCalculator.calculate(notNull(BigDecimal.class), notNull(BigDecimal.class), anyDouble())).thenReturn(changedPeriod);

        changedPlanPaymentSum = BigDecimal.valueOf(12000);
        mockedPlanPaymentCalculator = mock(IPlanPaymentCalculator.class);
        when(mockedPlanPaymentCalculator.calculate(notNull(BigDecimal.class), anyInt(), anyDouble())).thenReturn(changedPlanPaymentSum);

        payment = new AdvancePayment(paymentSum, paymentDate, AdvancePaymentType.PaymentDecreasing);
        creditEvent = new AdvancePaymentEvent(paymentDate, payment, creditHelper.getPlanPayment().getStartDate(),
                mockedPercentSumCalculator, mockedPeriodCalculator, mockedPlanPaymentCalculator);

        context = new CreditContext(creditHelper.getCredit());
        context.goToNextPaymentPeriod();
    }

    @Test
    public void testHandle() throws CreditCalculationException {
        PaymentGraphEvent paymentGraphEvent = creditEvent.handle(context);

        assertEquals(1, paymentGraphEvent.getPaymentPeriodNumber());
        assertSame(payment.getDate(), paymentGraphEvent.getDate());
        assertSame(payment.getSum(), paymentGraphEvent.getPaymentSum());

        assertSame(mockedPercentSum, paymentGraphEvent.getPercentSum());

        BigDecimal mainDebtSum = payment.getSum().subtract(mockedPercentSum);
        assertEquals(mainDebtSum, paymentGraphEvent.getMainDebtSum());

        BigDecimal remainMainDebtSum = creditHelper.getCredit().getSum().subtract(mainDebtSum);
        assertEquals(remainMainDebtSum, paymentGraphEvent.getRemainMainDebtSum());
        assertEquals("Досрочный платёж", paymentGraphEvent.getDescription());
    }

    @Test
    public void testHandleWithPaymentDecreasing() throws CreditCalculationException {
        creditEvent.handle(context);
        assertEquals(creditHelper.getCredit().getPeriodInMonth(), context.getRemainPeriodInMonth());
        assertNotSame(changedPlanPaymentSum, context.getCurrentPlanPayment());
        assertEquals(changedPlanPaymentSum, context.getCurrentPlanPayment().getSum());
    }

    @Test
    public void testHandleWithPeriodDecreasing() throws CreditCalculationException {
        payment.setType(AdvancePaymentType.PeriodDecreasing);
        creditEvent.handle(context);
        assertSame(creditHelper.getPlanPayment(), context.getCurrentPlanPayment());
        assertEquals(changedPeriod, context.getRemainPeriodInMonth());
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testHandleIfPaymentSumLessThanPercentSum() throws CreditCalculationException {
        payment.setSum(BigDecimal.valueOf(5));
        expectedException.expect(CreditCalculationException.class);
        expectedException.expectMessage(String.format("Сумма досрочного платежа %.2f не покрывает проценты", payment.getSum()));
        creditEvent.handle(context);
    }

    @Test
    public void testHandleWithDelayedRecalculation() throws CreditCalculationException {
        AdvancePaymentEvent mockedEvent = mock(AdvancePaymentEvent.class);
        mockedEvent.setIsDelayedRecalculation(true);
        mockedEvent.handle(context);
        verify(mockedEvent, never()).handleAdvancePaymentByType(context);
    }

    @Test
    public void testThatNewPeriodNotSetIfItGreaterThanCurrent() throws CreditCalculationException {
        payment.setType(AdvancePaymentType.PeriodDecreasing);
        int changedPeriod = creditHelper.getCredit().getPeriodInMonth() + 1;
        when(mockedPeriodCalculator.calculate(notNull(BigDecimal.class), notNull(BigDecimal.class), anyDouble())).thenReturn(changedPeriod);
        creditEvent.handle(context);
        assertNotEquals(changedPeriod, context.getRemainPeriodInMonth());
        assertEquals(creditHelper.getCredit().getPeriodInMonth(), context.getRemainPeriodInMonth());
    }

    @Test
    public void testThatNewPlanPaymentNotSetIfItGreaterThanCurrent() throws CreditCalculationException {
        changedPlanPaymentSum = creditHelper.getPlanPayment().getSum().add(BigDecimal.ONE);
        when(mockedPlanPaymentCalculator.calculate(notNull(BigDecimal.class), anyInt(), anyDouble())).thenReturn(changedPlanPaymentSum);
        creditEvent.handle(context);
        assertNotEquals(changedPlanPaymentSum, context.getCurrentPlanPayment().getSum());
        assertEquals(creditHelper.getPlanPayment().getSum(), context.getCurrentPlanPayment().getSum());
    }

    @Test
    public void testThatPlanPaymentAreSetToZeroIfLessThanZero() throws CreditCalculationException {
        changedPlanPaymentSum = BigDecimal.ONE.negate();
        when(mockedPlanPaymentCalculator.calculate(notNull(BigDecimal.class), anyInt(), anyDouble())).thenReturn(changedPlanPaymentSum);
        creditEvent.handle(context);
        assertEquals(BigDecimal.ZERO, context.getCurrentPlanPayment().getSum());
    }
}
