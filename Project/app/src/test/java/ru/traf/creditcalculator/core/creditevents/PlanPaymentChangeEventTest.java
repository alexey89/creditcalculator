package ru.traf.creditcalculator.core.creditevents;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.CreditContext;
import ru.traf.test.helpers.CreditHelperForTests;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.PlanPayment;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class PlanPaymentChangeEventTest {
    private CreditHelperForTests creditHelper;

    @Before
    public void initialize() {
        creditHelper = new CreditHelperForTests();
    }

    @Test
    public void testHandle() {
        ICreditContext context = new CreditContext(creditHelper.getCredit());
        context.goToNextPaymentPeriod();

        assertSame(creditHelper.getPlanPayment(), context.getCurrentPlanPayment());

        PlanPayment newPlanPayment = new PlanPayment(BigDecimal.valueOf(14000), Calendar.getInstance());
        Calendar creditEventDate = new GregorianCalendar(2016, Calendar.JANUARY, 31);
        PlanPaymentChangeEvent creditEvent = new PlanPaymentChangeEvent(creditEventDate, newPlanPayment);
        PaymentGraphEvent paymentGraphEvent = creditEvent.handle(context);

        assertSame(newPlanPayment, context.getCurrentPlanPayment());
        assertSame(creditEventDate, paymentGraphEvent.getDate());
        assertEquals(1, paymentGraphEvent.getPaymentPeriodNumber());
        assertEquals(String.format("Изменение планового платежа с %.2f на %.2f", creditHelper.getPlanPayment().getSum(), newPlanPayment.getSum())
                , paymentGraphEvent.getDescription());
    }
}
