package ru.traf.creditcalculator.core.creditevents;

import org.junit.Test;
import org.mockito.Matchers;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.PlanPayment;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;

public class CreditEventTest {
    @Test
    public void testCompareTo() {
        PlanPayment stubPlanPayment = new PlanPayment(BigDecimal.TEN);
        PlanPaymentChangeEvent event1 = new PlanPaymentChangeEvent(new GregorianCalendar(2016, 1, 1), stubPlanPayment);
        PlanPaymentChangeEvent event2 = new PlanPaymentChangeEvent(new GregorianCalendar(2015, 12, 31), stubPlanPayment);
        assertEquals(1, event1.compareTo(event2));

        event2 = new PlanPaymentChangeEvent(new GregorianCalendar(2016, 1, 2), stubPlanPayment);
        assertEquals(-1, event1.compareTo(event2));

        event2 = new PlanPaymentChangeEvent(new GregorianCalendar(2016, 1, 1), stubPlanPayment);
        assertEquals(0, event1.compareTo(event2));

        event1.setPriority((byte) 1);
        event2.setPriority((byte) 2);
        assertEquals(-1, event1.compareTo(event2));

        event2.setPriority((byte) 0);
        assertEquals(1, event1.compareTo(event2));
    }
}
