package ru.traf.creditcalculator.core.common.helpers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MathHelperTest {
    private double precision = 1e-6;

    @Test
    public void testRound() {
        double number = 5435.764756867;
        assertEquals(5435.8, MathHelper.round(number, 1), precision);
        assertEquals(5435.76, MathHelper.round(number, 2), precision);
        assertEquals(5435.765, MathHelper.round(number, 3), precision);
        assertEquals(5436, MathHelper.round(number, 0), precision);
        assertEquals(5435.7648, MathHelper.round(number, 4), precision);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRoundIfDecimalsLessThanZero() {
        MathHelper.round(100, -1);
    }

    @Test
    public void testLogOfBase() {
        assertEquals(3.3219, MathHelper.round(MathHelper.logOfBase(10, 2), 4), precision);
        assertEquals(10013.9520, MathHelper.round(MathHelper.logOfBase(22224.45, 1.001), 4), precision);
    }
}
