package ru.traf.creditcalculator.core.common.helpers;

import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

public class CalendarHelperTest {
    @Test
    public void testGetDateDifferenceInDays() {
        Calendar startDate = new GregorianCalendar(2016, Calendar.JANUARY, 1);
        Calendar endDate = new GregorianCalendar(2016, Calendar.JANUARY, 2);
        assertEquals(1, CalendarHelper.getDateDifferenceInDays(startDate, endDate));

        Calendar startDateWithTwoHours = new GregorianCalendar(2016, Calendar.JANUARY, 1, 2, 0);
        assertEquals(0, CalendarHelper.getDateDifferenceInDays(startDateWithTwoHours, endDate));

        endDate = new GregorianCalendar(2017, Calendar.JANUARY, 31);
        assertEquals(396, CalendarHelper.getDateDifferenceInDays(startDate, endDate));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDateDifferenceInDaysIfStartDateIsNull() {
        CalendarHelper.getDateDifferenceInDays(null, new GregorianCalendar(2016, Calendar.JANUARY, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDateDifferenceInDaysIfEndDateIsNull() {
        CalendarHelper.getDateDifferenceInDays(new GregorianCalendar(2016, Calendar.JANUARY, 1), null);
    }

    @Test
    public void testSetLastDayOfMonth() {
        Calendar date = new GregorianCalendar(2016, Calendar.JANUARY, 5);
        CalendarHelper.setLastDayOfMonth(date);
        assertEquals(31, date.get(Calendar.DAY_OF_MONTH));

        date = new GregorianCalendar(2016, Calendar.FEBRUARY, 10);
        CalendarHelper.setLastDayOfMonth(date);
        assertEquals(29, date.get(Calendar.DAY_OF_MONTH));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetLastDayOfMonthIdDateIsNull() {
        CalendarHelper.setLastDayOfMonth(null);
    }
}