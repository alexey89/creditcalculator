package ru.traf.creditcalculator.core;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class PlanPaymentTest {
    @Test
    public void testClone() {
        PlanPayment planPayment = new PlanPayment(BigDecimal.valueOf(100000), new GregorianCalendar(2016, Calendar.JANUARY, 31));
        planPayment.setIsLastDayOfMonth(true);
        planPayment.setTransferDaysOff(true);
        PlanPayment planPaymentClone = planPayment.clone();

        assertNotSame(planPayment, planPaymentClone);
        assertNotSame(planPayment.getStartDate(), planPaymentClone.getStartDate());
        assertEquals(planPayment.getStartDate(), planPaymentClone.getStartDate());
        assertEquals(planPayment.getSum(), planPaymentClone.getSum());
        assertEquals(planPayment.isLastDayOfMonth(), planPaymentClone.isLastDayOfMonth());
        assertEquals(planPayment.isTransferDaysOff(), planPaymentClone.isTransferDaysOff());
    }
}
