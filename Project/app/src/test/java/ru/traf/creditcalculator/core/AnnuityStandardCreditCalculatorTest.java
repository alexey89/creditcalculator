package ru.traf.creditcalculator.core;


import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import ru.traf.creditcalculator.core.common.SpecialDay;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraph;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.percentsumcalculators.StandardPercentSumCalculator;
import ru.traf.creditcalculator.core.periodcalculators.AnnuityPeriodCalculator;
import ru.traf.creditcalculator.core.planpaymentcalculators.AnnuityPlanPaymentCalculator;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;
import ru.traf.test.helpers.PaymentGraphCsvHelper;

import static org.junit.Assert.assertEquals;

public class AnnuityStandardCreditCalculatorTest {
    private Credit creditOne;
    private PlanPayment planPaymentOne;
    ICreditCalculator creditCalculatorOne;

    private Credit creditTwo;
    private PlanPayment planPaymentTwo;
    ICreditCalculator creditCalculatorTwo;

    private IPlanPaymentCalculator planPaymentCalculator;

    @Before
    public void initialize() {
        Calendar creditDate = new GregorianCalendar(2016, Calendar.JANUARY, 20);
        BigDecimal creditSum = BigDecimal.valueOf(100000);
        planPaymentCalculator = new AnnuityPlanPaymentCalculator();

        int creditPeriodOne = 5;
        Rate rateOne = new Rate(BigDecimal.TEN, creditDate);
        planPaymentOne = new PlanPayment(planPaymentCalculator.calculate(creditSum, creditPeriodOne, rateOne.getMonthRate()), new GregorianCalendar(2016, Calendar.FEBRUARY, 20));
        creditOne = new Credit("TestOne", CreditType.Annuity, creditSum, creditPeriodOne, rateOne, planPaymentOne);
        creditCalculatorOne = new AnnuityStandardCreditCalculator(creditOne, new StandardPercentSumCalculator(), planPaymentCalculator, new AnnuityPeriodCalculator());

        int creditPeriodTwo = 12;
        Rate rateTwo = new Rate(BigDecimal.valueOf(11.3), creditDate);
        planPaymentTwo = new PlanPayment(planPaymentCalculator.calculate(creditSum, creditPeriodTwo, rateTwo.getMonthRate()), new GregorianCalendar(2016, Calendar.FEBRUARY, 20));
        creditTwo = new Credit("TestTwo", CreditType.Annuity, creditSum, creditPeriodTwo, rateTwo, planPaymentTwo);
        creditCalculatorTwo = new AnnuityStandardCreditCalculator(creditTwo, new StandardPercentSumCalculator(), planPaymentCalculator, new AnnuityPeriodCalculator());
    }

    @Test
    public void testCalculateStandard() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateStandard.csv");
        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateStandard.csv");
    }

    @Test
    public void testCalculateWithDaysOffTransfer() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithDaysOffTransfer.csv");
        planPaymentOne.setTransferDaysOff(true);
        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithDaysOffTransfer.csv");

        paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithDaysOffTransfer_(CustomSpecialDays).csv");
        creditOne.addDayOff(new SpecialDay(new GregorianCalendar(2016, Calendar.FEBRUARY, 20)));
        creditOne.addWorkDay(new SpecialDay(new GregorianCalendar(2016, Calendar.FEBRUARY, 21)));
        paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithDaysOffTransfer_(CustomSpecialDays).csv");
    }

    @Test
    public void testCalculateIfPaymentOnLastDayOfMonth() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateIfPaymentOnLastDayOfMonth.csv");
        planPaymentOne.setIsLastDayOfMonth(true);
        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateIfPaymentOnLastDayOfMonth.csv");

        planPaymentOne.setTransferDaysOff(true);
        creditOne.addDayOff(new SpecialDay(new GregorianCalendar(2016, Calendar.APRIL, 30)));
        paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateIfPaymentOnLastDayOfMonth_(WithTransferDaysOff).csv");
        paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateIfPaymentOnLastDayOfMonth_(WithTransferDaysOff).csv");
    }

    @Test
    public void testCalculateWithFirstPaymentOnlyPercent() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithFirstPaymentOnlyPercent.csv");
        creditOne.setFirstPaymentOnlyPercent(true);
        creditOne.setCreditDate(new GregorianCalendar(2016, Calendar.FEBRUARY, 10));
        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithFirstPaymentOnlyPercent.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsPaymentDecreasingOnPlanPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsPaymentDecreasingOnPlanPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PaymentDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(12654.78), new GregorianCalendar(2016, Calendar.MAY, 20), AdvancePaymentType.PaymentDecreasing);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsPaymentDecreasingOnPlanPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsPeriodDecreasingOnPlanPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsPeriodDecreasingOnPlanPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(6354.47), new GregorianCalendar(2016, Calendar.APRIL, 20), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsPeriodDecreasingOnPlanPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsMixedDecreasingOnPlanPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsMixedDecreasingOnPlanPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(35000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(4643.65), new GregorianCalendar(2016, Calendar.APRIL, 18), AdvancePaymentType.PaymentDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(18000), new GregorianCalendar(2016, Calendar.MAY, 20), AdvancePaymentType.PeriodDecreasing);
        payment.setSubtractPlanPaymentSum(true);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsMixedDecreasingOnPlanPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPlanPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPlanPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(35000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(10000), new GregorianCalendar(2016, Calendar.MARCH, 11), AdvancePaymentType.PaymentDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(3000), new GregorianCalendar(2016, Calendar.MARCH, 18), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPlanPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsPaymentDecreasingOnPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsPaymentDecreasingOnPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PaymentDecreasing);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(12654.78), new GregorianCalendar(2016, Calendar.MAY, 18), AdvancePaymentType.PaymentDecreasing);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsPaymentDecreasingOnPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsPeriodDecreasingOnPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsPeriodDecreasingOnPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(12654.78), new GregorianCalendar(2016, Calendar.APRIL, 18), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsPeriodDecreasingOnPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsMixedDecreasingOnPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsMixedDecreasingOnPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(25000), new GregorianCalendar(2016, Calendar.APRIL, 18), AdvancePaymentType.PaymentDecreasing);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsMixedDecreasingOnPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPaymentDate() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPaymentDate.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(35000), new GregorianCalendar(2016, Calendar.MARCH, 5), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(10000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PaymentDecreasing);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(12000), new GregorianCalendar(2016, Calendar.MARCH, 15), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPaymentDate.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsPaymentDecreasingOnPaymentDateIfNextPlanPaymentOnlyPercent() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsPaymentDecreasingOnPaymentDateIfNextPlanPaymentOnlyPercent.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(2354.28), new GregorianCalendar(2016, Calendar.MAY, 17), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(4239.25), new GregorianCalendar(2016, Calendar.MAY, 19), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(1765.64), new GregorianCalendar(2016, Calendar.JULY, 18), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorTwo.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsPaymentDecreasingOnPaymentDateIfNextPlanPaymentOnlyPercent.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsPeriodDecreasingOnPaymentDateIfNextPlanPaymentOnlyPercent() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsPeriodDecreasingOnPaymentDateIfNextPlanPaymentOnlyPercent.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(6654.78), new GregorianCalendar(2016, Calendar.APRIL, 18), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsPeriodDecreasingOnPaymentDateIfNextPlanPaymentOnlyPercent.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPaymentDateIfNextPlanPaymentOnlyPercent() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPaymentDateIfNextPlanPaymentOnlyPercent.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(20000), new GregorianCalendar(2016, Calendar.MARCH, 5), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(10000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(20000), new GregorianCalendar(2016, Calendar.MARCH, 15), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorTwo.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentsMixedDecreasingInSamePaymentPeriodOnPaymentDateIfNextPlanPaymentOnlyPercent.csv");
    }

    @Test
    public void testCalculateWithMixedAdvancePaymentsTypeAndMixedDecreasing() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithMixedAdvancePaymentsTypeAndMixedDecreasing.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(15000), new GregorianCalendar(2016, Calendar.MARCH, 5), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(5000), new GregorianCalendar(2016, Calendar.MARCH, 10), AdvancePaymentType.PaymentDecreasing);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(10000), new GregorianCalendar(2016, Calendar.MARCH, 15), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(5000), new GregorianCalendar(2016, Calendar.MAY, 5), AdvancePaymentType.PaymentDecreasing);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(10000), new GregorianCalendar(2016, Calendar.MAY, 10), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(8000), new GregorianCalendar(2016, Calendar.MAY, 15), AdvancePaymentType.PeriodDecreasing);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(3000), new GregorianCalendar(2016, Calendar.JULY, 5), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(5000), new GregorianCalendar(2016, Calendar.JULY, 10), AdvancePaymentType.PeriodDecreasing);
        creditTwo.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(2500), new GregorianCalendar(2016, Calendar.JULY, 15), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditTwo.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorTwo.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithMixedAdvancePaymentsTypeAndMixedDecreasing.csv");
    }

    @Test
    public void testCalculateWithAdvancePaymentsBeforeFirstPaymentPeriodAndAfterLastPaymentPeriod() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateStandard.csv");

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.JANUARY, 19), AdvancePaymentType.PaymentDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2015, Calendar.DECEMBER, 31), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(60000), new GregorianCalendar(2016, Calendar.JANUARY, 5), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2015, Calendar.DECEMBER, 10), AdvancePaymentType.PaymentDecreasing);
        creditOne.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.JUNE, 20), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
    }

    @Test
    public void testCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PaymentDecreasingOnPlanPaymentDate.csv");
        creditOne.setFirstPaymentOnlyPercent(true);
        creditOne.setCreditDate(new GregorianCalendar(2016, Calendar.JANUARY, 31));

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.JANUARY, 31), AdvancePaymentType.PaymentDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PaymentDecreasingOnPlanPaymentDate.csv");

        paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PeriodDecreasingOnPlanPaymentDate.csv");
        creditOne.getAdvancePayments().clear();

        payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2016, Calendar.JANUARY, 31), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        creditOne.addAdvancePayment(payment);

        paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PeriodDecreasingOnPlanPaymentDate.csv");

        paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PaymentDecreasingOnPaymentDate.csv");
        creditOne.getAdvancePayments().clear();

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.JANUARY, 31), AdvancePaymentType.PaymentDecreasing);
        creditOne.addAdvancePayment(payment);

        paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PaymentDecreasingOnPaymentDate.csv");

        paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PeriodDecreasingOnPaymentDate.csv");
        creditOne.getAdvancePayments().clear();

        payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2016, Calendar.JANUARY, 31), AdvancePaymentType.PeriodDecreasing);
        creditOne.addAdvancePayment(payment);

        paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PeriodDecreasingOnPaymentDate.csv");

        paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PaymentDecreasingOnPaymentDate.csv");
        creditOne.getAdvancePayments().clear();

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2016, Calendar.JANUARY, 31), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditOne.addAdvancePayment(payment);

        paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);

        paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateWithAdvancePaymentOnFirstPaymentPeriodIfFirstPaymentOnlyPercent_PeriodDecreasingOnPaymentDate.csv");
        creditOne.getAdvancePayments().clear();

        payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2016, Calendar.JANUARY, 31), AdvancePaymentType.PeriodDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        creditOne.addAdvancePayment(payment);

        paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
    }

    @Test
    public void testChangePercentRate() throws CreditCalculationException, IOException, ParseException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestChangePercentRate.csv");

        Rate rate = new Rate(BigDecimal.valueOf(8), new GregorianCalendar(2016, Calendar.JUNE, 1));
        creditOne.addRate(rate);

        rate = new Rate(BigDecimal.valueOf(9.5), new GregorianCalendar(2016, Calendar.MARCH, 20));
        creditOne.addRate(rate);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestChangePercentRate.csv");
    }

    @Test
    public void testChangePercentRateWithMaximumPriority() throws CreditCalculationException, IOException, ParseException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("testChangePercentRateWithMaximumPriority.csv");

        Rate rate = new Rate(BigDecimal.valueOf(8), new GregorianCalendar(2016, Calendar.JUNE, 1));
        rate.setApplyRateBeforePayment(true);
        creditOne.addRate(rate);

        rate = new Rate(BigDecimal.valueOf(9.5), new GregorianCalendar(2016, Calendar.MARCH, 20));
        rate.setApplyRateBeforePayment(true);
        creditOne.addRate(rate);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestChangePercentRateWithMaximumPriority.csv");
    }

    @Test
    public void testChangePercentRateWithoutPlanPaymentRecalculation() throws CreditCalculationException, IOException, ParseException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestChangePercentRateWithoutPlanPaymentRecalculation.csv");

        Rate rate = new Rate(BigDecimal.valueOf(9.5), new GregorianCalendar(2016, Calendar.MARCH, 20));
        rate.setRecalculatePlanPayment(false);
        creditOne.addRate(rate);

        rate = new Rate(BigDecimal.valueOf(8), new GregorianCalendar(2016, Calendar.MAY, 1));
        rate.setRecalculatePlanPayment(false);
        creditOne.addRate(rate);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestChangePercentRateWithoutPlanPaymentRecalculation.csv");
    }

    @Test
    public void testChangePercentRateInFirstPaymentPeriod() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestChangePercentRateInFirstPaymentPeriod.csv");

        Rate rate = new Rate(BigDecimal.valueOf(8), new GregorianCalendar(2016, Calendar.JANUARY, 20));
        creditOne.addRate(rate);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestChangePercentRateInFirstPaymentPeriod.csv");
    }

    @Test
    public void testChangePercentRateBeforeFirstPaymentPeriodAndAfterLastPaymentPeriod() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestChangePercentRateBeforeFirstPaymentPeriodAndAfterLastPaymentPeriod.csv");

        Rate rate = new Rate(BigDecimal.valueOf(9.5), new GregorianCalendar(2015, Calendar.DECEMBER, 20));
        creditOne.addRate(rate);

        rate = new Rate(BigDecimal.valueOf(8), new GregorianCalendar(2016, Calendar.JANUARY, 19));
        creditOne.addRate(rate);

        rate = new Rate(BigDecimal.valueOf(7), new GregorianCalendar(2016, Calendar.JUNE, 20));
        creditOne.addRate(rate);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestChangePercentRateBeforeFirstPaymentPeriodAndAfterLastPaymentPeriod.csv");
    }

    @Test
    public void testChangePlanPayment() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestChangePlanPayment.csv");

        PlanPayment payment = new PlanPayment(BigDecimal.valueOf(15678.34), new GregorianCalendar(2016, Calendar.MARCH, 20));
        creditOne.addPlanPayment(payment);

        payment = new PlanPayment(BigDecimal.valueOf(12000), new GregorianCalendar(2016, Calendar.JUNE, 10));
        creditOne.addPlanPayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestChangePlanPayment.csv");
    }

    @Test
    public void testChangePlanPaymentBeforeFirstPaymentPeriodAndAfterLastPaymentPeriod() throws IOException, ParseException, CreditCalculationException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestChangePlanPaymentBeforeFirstPaymentPeriodAndAfterLastPaymentPeriod.csv");

        PlanPayment payment = new PlanPayment(BigDecimal.valueOf(20000), new GregorianCalendar(2016, Calendar.FEBRUARY, 10));
        creditOne.addPlanPayment(payment);

        payment = new PlanPayment(BigDecimal.valueOf(12000), new GregorianCalendar(2016, Calendar.JULY, 11));
        creditOne.addPlanPayment(payment);

        PaymentGraph paymentGraph = creditCalculatorOne.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestChangePlanPaymentBeforeFirstPaymentPeriodAndAfterLastPaymentPeriod.csv");
    }

    @Test(expected = CreditCalculationException.class)
    public void testChangePlanPaymentIfItNotCoverPercent() throws IOException, ParseException, CreditCalculationException {
        PlanPayment payment = new PlanPayment(BigDecimal.valueOf(500), new GregorianCalendar(2016, Calendar.MARCH, 20));
        creditOne.addPlanPayment(payment);
        creditCalculatorOne.calculate();
    }

    @Test
    public void testRealSberbank() throws CreditCalculationException, IOException, ParseException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestRealSberbank.csv");

        Calendar creditDate = new GregorianCalendar(2014, Calendar.FEBRUARY, 6);
        BigDecimal creditSum = BigDecimal.valueOf(999202);
        int creditPeriod = 120;
        Rate rate = new Rate(BigDecimal.valueOf(12.5), creditDate);
        BigDecimal planPaymentSum = planPaymentCalculator.calculate(creditSum, creditPeriod, rate.getMonthRate());
        PlanPayment planPayment = new PlanPayment(planPaymentSum, new GregorianCalendar(2014, Calendar.MARCH, 6));
        Credit credit = new Credit("Тест Сбербанк", CreditType.Annuity, creditSum, creditPeriod, rate, planPayment);

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(20000), new GregorianCalendar(2014, Calendar.MARCH, 17), AdvancePaymentType.PaymentDecreasing);
        payment.setOnlyPercentForNextPlanPayment(true);
        credit.addAdvancePayment(payment);

        ICreditCalculator creditCalculator = new AnnuityStandardCreditCalculator(credit, new StandardPercentSumCalculator(), planPaymentCalculator, new AnnuityPeriodCalculator());
        PaymentGraph paymentGraph = creditCalculator.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestRealSberbank.csv");
    }

    @Test
    public void testCalculateOurCredit() throws IOException, CreditCalculationException, ParseException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateOurCredit.csv");

        Calendar creditDate = new GregorianCalendar(2013, Calendar.JUNE, 5);
        BigDecimal creditSum = BigDecimal.valueOf(1121000);
        int creditPeriod = 180;
        Rate rate = new Rate(BigDecimal.valueOf(13.5), creditDate);
        BigDecimal planPaymentSum = planPaymentCalculator.calculate(creditSum, creditPeriod, rate.getMonthRate());
        PlanPayment planPayment = new PlanPayment(planPaymentSum, new GregorianCalendar(2013, Calendar.JUNE, 20));
        Credit credit = new Credit("Наша хата", CreditType.Annuity, creditSum, creditPeriod, rate, planPayment);
        credit.setFirstPaymentOnlyPercent(true);
        credit.setCreditDate(creditDate);

        AdvancePayment payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2013, Calendar.SEPTEMBER, 19), AdvancePaymentType.PaymentDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2013, Calendar.OCTOBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2013, Calendar.NOVEMBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2013, Calendar.DECEMBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30600), new GregorianCalendar(2014, Calendar.JANUARY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2014, Calendar.FEBRUARY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2014, Calendar.MARCH, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2014, Calendar.APRIL, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2014, Calendar.MAY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2014, Calendar.JUNE, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        Rate newRate = new Rate(BigDecimal.valueOf(11.5), new GregorianCalendar(2014, Calendar.JUNE, 21));
        credit.addRate(newRate);

        PlanPayment newPlanPayment = new PlanPayment(BigDecimal.valueOf(13000.84), new GregorianCalendar(2014, Calendar.JULY, 20));
        credit.addPlanPayment(newPlanPayment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2014, Calendar.JULY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2014, Calendar.AUGUST, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(28000), new GregorianCalendar(2014, Calendar.NOVEMBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(20070), new GregorianCalendar(2014, Calendar.DECEMBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2015, Calendar.JANUARY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(25000), new GregorianCalendar(2015, Calendar.FEBRUARY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(25000), new GregorianCalendar(2015, Calendar.MARCH, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2015, Calendar.APRIL, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(30000), new GregorianCalendar(2015, Calendar.MAY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2015, Calendar.JUNE, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(100000), new GregorianCalendar(2015, Calendar.JULY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2015, Calendar.AUGUST, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(40000), new GregorianCalendar(2015, Calendar.SEPTEMBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(50000), new GregorianCalendar(2015, Calendar.OCTOBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(60000), new GregorianCalendar(2015, Calendar.NOVEMBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(70000), new GregorianCalendar(2015, Calendar.DECEMBER, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(70000), new GregorianCalendar(2016, Calendar.JANUARY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(55000), new GregorianCalendar(2016, Calendar.FEBRUARY, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        payment = new AdvancePayment(BigDecimal.valueOf(40000), new GregorianCalendar(2016, Calendar.MARCH, 19), AdvancePaymentType.PeriodDecreasing);
        payment.setPayOnPlanPaymentDate(true);
        payment.setSubtractPlanPaymentSum(true);
        credit.addAdvancePayment(payment);

        ICreditCalculator creditCalculator = new AnnuityStandardCreditCalculator(credit, new StandardPercentSumCalculator(), planPaymentCalculator, new AnnuityPeriodCalculator());
        PaymentGraph paymentGraph = creditCalculator.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateOurCredit.csv");
    }

    @Test
    public void testCalculateBrotherCredit() throws IOException, CreditCalculationException, ParseException {
        PaymentGraph paymentGraphFromCsv = PaymentGraphCsvHelper.importFromCsv("TestCalculateBrotherCredit.csv");

        Calendar creditDate = new GregorianCalendar(2016, Calendar.FEBRUARY, 24);
        BigDecimal creditSum = BigDecimal.valueOf(469958);
        int creditPeriod = 300;
        Rate rate = new Rate(BigDecimal.valueOf(11.4), creditDate);
        BigDecimal planPaymentSum = planPaymentCalculator.calculate(creditSum, creditPeriod, rate.getMonthRate());
        PlanPayment planPayment = new PlanPayment(planPaymentSum, new GregorianCalendar(2016, Calendar.MARCH, 24));
        Credit credit = new Credit("Братова хата", CreditType.Annuity, creditSum, creditPeriod, rate, planPayment);
        credit.setCreditDate(creditDate);

        ICreditCalculator creditCalculator = new AnnuityStandardCreditCalculator(credit, new StandardPercentSumCalculator(), planPaymentCalculator, new AnnuityPeriodCalculator());
        PaymentGraph paymentGraph = creditCalculator.calculate();
        comparePaymentGraph(paymentGraphFromCsv, paymentGraph);
//        PaymentGraphCsvHelper.exportToCsv(paymentGraph, "TestCalculateBrotherCredit.csv");
    }

    private void comparePaymentGraph(PaymentGraph paymentGraph1, PaymentGraph paymentGraph2) {
        List<PaymentGraphEvent> events1 = Stream.of(paymentGraph1.getPaymentGraphEvents()).collect(Collectors.toList());
        List<PaymentGraphEvent> events2 = Stream.of(paymentGraph2.getPaymentGraphEvents()).collect(Collectors.toList());
        for (int i = 0; i < events1.size(); i++) {
            PaymentGraphEvent event1 = events1.get(i);
            PaymentGraphEvent event2 = events2.get(i);
            assertEquals(event1.getPaymentPeriodNumber(), event2.getPaymentPeriodNumber());
            assertEquals(event1.getDate(), event2.getDate());
            assertEquals(event1.getPaymentSum(), event2.getPaymentSum() != null ? event2.getPaymentSum().setScale(2, RoundingMode.HALF_UP) : null);
            assertEquals(event1.getPercentSum(), event2.getPercentSum() != null ? event2.getPercentSum().setScale(2, RoundingMode.HALF_UP) : null);
            assertEquals(event1.getMainDebtSum(), event2.getMainDebtSum() != null ? event2.getMainDebtSum().setScale(2, RoundingMode.HALF_UP) : null);
            assertEquals(event1.getRemainMainDebtSum(), event2.getRemainMainDebtSum() != null ? event2.getRemainMainDebtSum().setScale(2, RoundingMode.HALF_UP) : null);
            assertEquals(event1.getDescription(), event2.getDescription());
            assertEquals(event1.getEventType(), event2.getEventType());
        }
    }
}
