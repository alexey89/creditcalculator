package ru.traf.creditcalculator.core;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Calendar;

import ru.traf.creditcalculator.core.common.SpecialDay;
import ru.traf.creditcalculator.core.common.helpers.SpecialDayHelper;

/**
 * Кредит
 */
public class Credit {
    /**
     * Наименование кредита
     */
    private String name;

    /**
     * Тип кредита
     */
    private CreditType type;

    /**
     * Сумма кредита
     */
    private BigDecimal sum;

    /**
     * Дата выдачи кредита
     */
    private Calendar creditDate;

    /**
     * Срок кредита в месяцах
     */
    private int periodInMonth;

    /**
     * Процентные ставки по кредиту
     */
    private Collection<Rate> rates;

    /**
     * Плановые платежи
     */
    private Collection<PlanPayment> planPayments;

    /**
     * Досрочные платежи
     */
    private Collection<AdvancePayment> advancePayments;

    /**
     * Первый платёж "Только проценты"
     */
    private boolean firstPaymentOnlyPercent;

    /**
     * Список выходных дней
     */
    private Collection<SpecialDay> daysOff;

    /**
     * Список праздничных дней
     */
    private Collection<SpecialDay> workDays;

    public Credit(@NonNull String name, @NonNull CreditType type, @NonNull BigDecimal sum, int periodInMonth, @NonNull Rate rate, @NonNull PlanPayment planPayment) {
        setName(name);
        setType(type);
        setSum(sum);
        setPeriodInMonth(periodInMonth);
        addRate(rate);
        addPlanPayment(planPayment);
        daysOff = SpecialDayHelper.getStandardDaysOff();
        workDays = new ArrayList<>();
    }

    /**
     * Добавляет процентную ставку к кредиту
     */
    public void addRate(@NonNull Rate rate) {
        if (rate == null) {
            throw new IllegalArgumentException("Процентная ставка не может быть равной null");
        }
        if (rates == null) {
            rates = new ArrayList<>();
        }
        rates.add(rate);
    }

    /**
     * Добавляет плановый платёж  к кредиту
     */
    public void addPlanPayment(@NonNull PlanPayment payment) {
        if (payment == null) {
            throw new IllegalArgumentException("Плановый платёж не может быть равным null");
        }
        if (planPayments == null) {
            planPayments = new ArrayList<>();
        }
        planPayments.add(payment);
    }

    /**
     * Добавляет досрочный платёж к кредиту
     */
    public void addAdvancePayment(@NonNull AdvancePayment payment) {
        if (payment == null) {
            return;
        }
        if (advancePayments == null) {
            advancePayments = new ArrayList<>();
        }
        advancePayments.add(payment);
    }

    /**
     * Добавляет выходной день к кредиту
     */
    public void addDayOff(@NonNull SpecialDay dayOff) {
        if (dayOff == null) {
            return;
        }

        if (daysOff == null) {
            daysOff = new ArrayList<>();
        }
        daysOff.add(dayOff);
    }

    /**
     * Добавляет рабочий день к кредиту
     */
    public void addWorkDay(@NonNull SpecialDay workDay) {
        if (workDay == null) {
            return;
        }

        if (workDays == null) {
            workDays = new ArrayList<>();
        }
        workDays.add(workDay);
    }

    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        if (name == null || name.trim().length() == 0) {
            throw new IllegalArgumentException("Имя кредита не может быть равным null или пустым");
        }
        this.name = name;
    }

    public CreditType getType() {
        return type;
    }

    public void setType(@NonNull CreditType type) {
        if (type == null) {
            throw new IllegalArgumentException("Тип кредита не может быть равным null");
        }
        this.type = type;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(@NonNull BigDecimal sum) {
        if (sum == null) {
            throw new IllegalArgumentException("Сумма кредита не может быть равна null");
        }
        if (sum.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Сумма кредита должна быть больше нуля");
        }
        this.sum = sum;
    }

    public Calendar getCreditDate() {
        return creditDate;
    }

    public void setCreditDate(Calendar creditDate) {
        this.creditDate = creditDate;
    }

    public int getPeriodInMonth() {
        return periodInMonth;
    }

    public void setPeriodInMonth(int periodInMonth) {
        if (periodInMonth <= 0) {
            throw new IllegalArgumentException("Период кредита должен быть больше нуля");
        }
        this.periodInMonth = periodInMonth;
    }

    public Collection<Rate> getRates() {
        return rates;
    }

    public Collection<PlanPayment> getPlanPayments() {
        return planPayments;
    }

    public Collection<AdvancePayment> getAdvancePayments() {
        return advancePayments;
    }

    public boolean isFirstPaymentOnlyPercent() {
        return firstPaymentOnlyPercent;
    }

    public void setFirstPaymentOnlyPercent(boolean firstPaymentOnlyPercent) {
        this.firstPaymentOnlyPercent = firstPaymentOnlyPercent;
    }

    public Collection<SpecialDay> getDaysOff() {
        return daysOff;
    }

    public Collection<SpecialDay> getWorkDays() {
        return workDays;
    }
}
