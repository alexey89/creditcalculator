package ru.traf.creditcalculator.core;

import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraph;

/**
 * Интерфейс кредитного калькулятора
 */
interface ICreditCalculator {
    /**
     * Сформировать график платежей
     */
    PaymentGraph calculate() throws CreditCalculationException;
}
