package ru.traf.creditcalculator.core;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Досрочный платёж
 */
public class AdvancePayment {
    /**
     * Сумма платежа
     */
     private BigDecimal sum;

    /**
     * Дата платежа
     */
    private Calendar date;

    /**
     * Тип платежа
     */
    private AdvancePaymentType type;

    /**
     * Платёж в дату планового платежа
     */
    private boolean payOnPlanPaymentDate;

    /**
     * Последущий плановый платёж "Только проценты"
     */
    private boolean onlyPercentForNextPlanPayment;

    /**
     * Вычесть из суммы платежа сумму планового платежа
     */
    private boolean subtractPlanPaymentSum;

    public AdvancePayment(@NonNull BigDecimal sum, @NonNull Calendar date, @NonNull AdvancePaymentType type) {
        setSum(sum);
        setDate(date);
        setType(type);
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(@NonNull BigDecimal sum) {
        if (sum == null) {
            throw new IllegalArgumentException("Сумма досрочного платежа не может быть равна null");
        }
        if (sum.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Сумма досрочного платежа должна быть больше нуля");
        }
        this.sum = sum;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        if (date == null) {
            throw new IllegalArgumentException("Дата досрочного платежа не может быть равна null");
        }
        this.date = date;
    }

    public AdvancePaymentType getType() {
        return type;
    }

    public void setType(@NonNull AdvancePaymentType type) {
        if (type == null) {
            throw new IllegalArgumentException("Тип досрочного платежа не может быть равен null");
        }
        this.type = type;
    }

    public boolean isPayOnPlanPaymentDate() {
        return payOnPlanPaymentDate;
    }

    public void setPayOnPlanPaymentDate(boolean payOnPlanPaymentDate) {
        this.payOnPlanPaymentDate = payOnPlanPaymentDate;
    }

    public boolean isOnlyPercentForNextPlanPayment() {
        return onlyPercentForNextPlanPayment;
    }

    public void setOnlyPercentForNextPlanPayment(boolean onlyPercentForNextPlanPayment) {
        this.onlyPercentForNextPlanPayment = onlyPercentForNextPlanPayment;
    }

    public boolean isSubtractPlanPaymentSum() {
        return subtractPlanPaymentSum;
    }

    public void setSubtractPlanPaymentSum(boolean subtractPlanPaymentSum) {
        this.subtractPlanPaymentSum = subtractPlanPaymentSum;
    }
}
