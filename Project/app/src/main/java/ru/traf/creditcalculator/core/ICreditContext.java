package ru.traf.creditcalculator.core;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Интерфейс для контекста кредита
 */
public interface ICreditContext {
    /**
     * Перейти к следующему платёжному периоду
     */
    void goToNextPaymentPeriod();

    /**
     * Получить изменения плановых платежей в текущем платёжном периоде
     */
    Iterable<PlanPayment> getCurrentPaymentPeriodPlanPaymentChanges();

    /**
     * Получить изменения процентных ставок в текущем платёжном периоде
     */
    Iterable<Rate> getCurrentPaymentPeriodRateChanges();

    /**
     * Получить досрочные платежи для текущего платёжного периода
     */
    Iterable<AdvancePayment> getCurrentPaymentPeriodAdvancePayments();

    /**
     * Получить оставшуюся сумму основного долга
     */
    BigDecimal getRemainMainDebtSum();

    /**
     * Уменьшить сумму основного долга
     * @param sum Сумма, на которую необходимо уменьшить основной долг
     */
    void decreaseRemainMainDebtSum(BigDecimal sum);

    /**
     * Получить оставшийся период в месяцах
     */
    int getRemainPeriodInMonth();

    /**
     * Установить новый период кредита в месяцах
     */
    void setRemainPeriodInMonth(int newPeriod);

    /**
     * Установить текущий плановый платёж
     */
    void setCurrentPlanPayment(PlanPayment payment);

    /**
     * Возвратить текущий плановый платёж
     */
    PlanPayment getCurrentPlanPayment();

    /**
     * Изменить сумму текущего планового платежа
     */
    void changeCurrentPlanPaymentSum(BigDecimal newSum);

    /**
     * Установить текущую процентную ставку
     */
    void setCurrentRate(Rate newRate);

    /**
     * Получить текущую процентную ставку
     */
    Rate getCurrentRate();

    /**
     * Получить дату следующего планового платежа
     */
    Calendar getNextPlanPaymentDate();

    /**
     * Получить дату предыдущего планового платежа
     */
    Calendar getPreviousPlanPaymentDate();

    /**
     * Получить номер текущего платёжного периода
     */
    int getCurrentPeriodNumber();

    /**
     * Является ли следующий плановый платёж первым
     */
    boolean isFirstPlanPayment();

    /**
     * Возвращает исходный кредит
     */
    Credit getCredit();
}
