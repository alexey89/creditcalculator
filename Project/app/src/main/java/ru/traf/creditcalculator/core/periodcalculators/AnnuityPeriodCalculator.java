package ru.traf.creditcalculator.core.periodcalculators;

import java.math.BigDecimal;

import ru.traf.creditcalculator.core.common.helpers.MathHelper;

/**
 * Расчётчик периода для кредита с аннуитетными платежами
 */
public class AnnuityPeriodCalculator implements IPeriodCalculator {
    @Override
    public int calculate(BigDecimal remainDebtSum, BigDecimal planPaymentSum, double monthRate) {
        return (int) Math.ceil(MathHelper.logOfBase(planPaymentSum.divide(planPaymentSum.subtract(BigDecimal.valueOf(monthRate).multiply(remainDebtSum)),
                BigDecimal.ROUND_HALF_UP).doubleValue(), 1 + monthRate));
    }
}
