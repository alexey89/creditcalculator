package ru.traf.creditcalculator.core.creditevents;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;

import ru.traf.creditcalculator.core.CreditEventType;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.Rate;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;

/**
 * Событие "Изменение процентной ставки"
 */
public class PercentRateChangeEvent extends CreditEvent {
    private static final byte DEFAULT_PRIORITY = 4;
    private static DecimalFormat decimalFormatter = new DecimalFormat("#.##");
    static CreditEventType eventType = CreditEventType.PercentRateChange;

    /**
     * Новая процентная ставка
     */
    private Rate rate;
    private IPlanPaymentCalculator planPaymentCalculator;

    public PercentRateChangeEvent(@NonNull Calendar date, @NonNull Rate rate, IPlanPaymentCalculator planPaymentCalculator) {
        super(date, DEFAULT_PRIORITY);
        this.planPaymentCalculator = planPaymentCalculator;
        setRate(rate);
    }

    @Override
    public final PaymentGraphEvent handle(ICreditContext context) {
        BigDecimal oldYearRateInPercent = context.getCurrentRate().getYearRateInPercent();
        context.setCurrentRate(rate);
        if (rate.isRecalculatePlanPayment() && context.getRemainPeriodInMonth() > 0) {
            BigDecimal newPlanPaymentSum = planPaymentCalculator.calculate(context.getRemainMainDebtSum(), context.getRemainPeriodInMonth(), context.getCurrentRate().getMonthRate());
            context.changeCurrentPlanPaymentSum(newPlanPaymentSum);
        }
        return new PaymentGraphEvent(context.getCurrentPeriodNumber(), getDate(), eventType,
                eventType.getDescription() + String.format(" с %s%% на %s%%", decimalFormatter.format(oldYearRateInPercent), decimalFormatter.format(rate.getYearRateInPercent())));
    }

    public Rate getRate() {
        return rate;
    }

    private void setRate(@NonNull Rate rate) {
        if (rate == null) {
            throw new IllegalArgumentException("Новая процентная ставка не может быть равна null");
        }
        this.rate = rate;
    }
}
