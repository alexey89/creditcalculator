package ru.traf.creditcalculator.core.creditevents;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

import ru.traf.creditcalculator.core.CreditEventType;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.PlanPayment;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;

/**
 * Событие "Изменение планового платежа"
 */
public class PlanPaymentChangeEvent extends CreditEvent {
    private static final byte DEFAULT_PRIORITY = 1;
    static CreditEventType eventType = CreditEventType.PlanPaymentChange;

    /**
     * Новый плановый платёж
     */
    protected PlanPayment payment;

    public PlanPaymentChangeEvent(@NonNull Calendar date, @NonNull PlanPayment payment) {
        super(date, DEFAULT_PRIORITY);
        setPayment(payment);
    }

    @Override
    public final PaymentGraphEvent handle(ICreditContext context) {
        BigDecimal oldPlanPaymentSum = context.getCurrentPlanPayment().getSum();
        context.setCurrentPlanPayment(payment);
        return new PaymentGraphEvent(context.getCurrentPeriodNumber(), getDate(), eventType,
                eventType.getDescription() + String.format(" с %.2f на %.2f", oldPlanPaymentSum, payment.getSum()));
    }

    public PlanPayment getPayment() {
        return payment;
    }

    private void setPayment(@NonNull PlanPayment payment) {
        if (payment == null) {
            throw new IllegalArgumentException("Новый плановый платёж не может быть равен null");
        }
        this.payment = payment;
    }
}
