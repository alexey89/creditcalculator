package ru.traf.creditcalculator.core.paymentgraph;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * График платежей
 */
public class PaymentGraph {
    /**
     * Список событий графика платежей
     */
    private Collection<PaymentGraphEvent> paymentGraphEvents = new ArrayList<>();

    /**
     * Добавить событие в график платежей
     */
    public void AddPaymentGraphEvent(PaymentGraphEvent event) {
        paymentGraphEvents.add(event);
    }

    public BigDecimal getTotalPaymentSum() {
        BigDecimal totalSum = BigDecimal.ZERO;
        for (PaymentGraphEvent event : paymentGraphEvents) {
            if (event.getPaymentSum() != null) {
                totalSum = totalSum.add(event.getPaymentSum());
            }
        }
        return totalSum;
    }

    public BigDecimal getTotalPercentSum() {
        BigDecimal percentSum = BigDecimal.ZERO;
        for (PaymentGraphEvent event : paymentGraphEvents) {
            if (event.getPercentSum() != null) {
                percentSum = percentSum.add(event.getPercentSum());
            }
        }
        return percentSum;
    }

    public BigDecimal getTotalMainDebtSum() {
        BigDecimal mainDebtSum = BigDecimal.ZERO;
        for (PaymentGraphEvent event : paymentGraphEvents) {
            if (event.getMainDebtSum() != null) {
                mainDebtSum = mainDebtSum.add(event.getMainDebtSum());
            }
        }
        return mainDebtSum;
    }

    public Iterable<PaymentGraphEvent> getPaymentGraphEvents() {
        return paymentGraphEvents;
    }
}
