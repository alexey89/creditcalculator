package ru.traf.creditcalculator.core.percentsumcalculators;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Интерфейс расчётчика процентов
 */
public interface IPercentSumCalculator {
    /**
     * Рассчитать сумму процентов
     * @param startDate Начальная дата
     * @param endDate Конечная дата
     * @param remainMainDebtSum Остаток основного долга
     * @param yearRate Процентная ставка
     */
    BigDecimal calculate(Calendar startDate, Calendar endDate, BigDecimal remainMainDebtSum, BigDecimal yearRate);
}
