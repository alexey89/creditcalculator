package ru.traf.creditcalculator.core;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Плановый платёж
 */
public class PlanPayment implements Cloneable {
    /**
     * Сумма платежа
     */
    private BigDecimal sum;

    /**
     * Дата вступления в силу
     */
    private Calendar startDate;

    /**
     * Производится ли платёж в последний день месяца
     */
    private boolean isLastDayOfMonth;

    /**
     * Перенос выходных дней
     */
    private boolean transferDaysOff;

    public PlanPayment(@NonNull BigDecimal sum, @NonNull Calendar startDate) {
        setSum(sum);
        setStartDate(startDate);
    }

    public PlanPayment(@NonNull BigDecimal sum) {
        setSum(sum);
        isLastDayOfMonth = true;
    }

    @Override
    protected PlanPayment clone() {
        PlanPayment clone = null;
        try {
            clone = (PlanPayment) super.clone();
            clone.startDate = (Calendar) startDate.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(@NonNull BigDecimal sum) {
        if (sum == null) {
            throw new IllegalArgumentException("Сумма планового платежа не может быть равна null");
        }
        if (sum.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Сумма планового платежа не может быть меньше нуля");
        }
        this.sum = sum;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(@NonNull Calendar startDate) {
        if (startDate == null) {
            throw new IllegalArgumentException("Дата планового платежа не может быть равна null");
        }
        this.startDate = startDate;
    }

    public boolean isLastDayOfMonth() {
        return isLastDayOfMonth;
    }

    public void setIsLastDayOfMonth(boolean isLastDayOfMonth) {
        this.isLastDayOfMonth = isLastDayOfMonth;
    }

    public boolean isTransferDaysOff() {
        return transferDaysOff;
    }

    public void setTransferDaysOff(boolean transferDaysOff) {
        this.transferDaysOff = transferDaysOff;
    }
}
