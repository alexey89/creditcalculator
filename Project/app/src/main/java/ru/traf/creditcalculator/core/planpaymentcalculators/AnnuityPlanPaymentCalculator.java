package ru.traf.creditcalculator.core.planpaymentcalculators;

import java.math.BigDecimal;

/**
 * Расчётчик аннуитетного планового платежа
 */
public class AnnuityPlanPaymentCalculator implements IPlanPaymentCalculator {
    @Override
    public BigDecimal calculate(BigDecimal creditSum, int creditPeriodInMouth, double monthRate) {
        return creditSum
                .multiply(BigDecimal.valueOf(monthRate))
                .multiply(BigDecimal.valueOf(Math.pow(1 + monthRate, creditPeriodInMouth)))
                .divide(BigDecimal.valueOf(Math.pow(1 + monthRate, creditPeriodInMouth) - 1), 2, BigDecimal.ROUND_HALF_UP);
    }
}
