package ru.traf.creditcalculator.core.creditevents;

import android.support.annotation.NonNull;

import com.annimon.stream.Stream;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import ru.traf.creditcalculator.core.CreditEventType;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.percentsumcalculators.IPercentSumCalculator;

/**
 * Событие "Плановый платёж"
 */
public class PlanPaymentEvent extends CreditEvent {
    private static final byte DEFAULT_PRIORITY = 2;
    static CreditEventType eventType = CreditEventType.PlanPayment;

    /**
     * Платить только проценты
     */
    private boolean onlyPercent;

    /**
     * Начальная дата, от которой рассчитывать проценты
     */
    private Calendar startPercentDate;
    private IPercentSumCalculator percentSumCalculator;

    private List<AdvancePaymentEvent> advancePaymentEventsWithDelayedRecalculation;

    public PlanPaymentEvent(@NonNull Calendar date, @NonNull Calendar startPercentDate, IPercentSumCalculator percentSumCalculator) {
        this(date, startPercentDate, false, percentSumCalculator);
    }

    public PlanPaymentEvent(@NonNull Calendar date, @NonNull Calendar startPercentDate, boolean onlyPercent, IPercentSumCalculator percentSumCalculator) {
        super(date, DEFAULT_PRIORITY);
        this.percentSumCalculator = percentSumCalculator;
        setStartPercentDate(startPercentDate);
        setOnlyPercent(onlyPercent);
    }

    @Override
    public final PaymentGraphEvent handle(ICreditContext context) throws CreditCalculationException {
        BigDecimal paymentSum = context.getCurrentPlanPayment().getSum();
        BigDecimal percentSum = percentSumCalculator.calculate(startPercentDate, getDate(), context.getRemainMainDebtSum(), context.getCurrentRate().getYearRate());
        int subtractPeriodCount = 1;
        if (onlyPercent) {
            paymentSum = percentSum;
            if (context.isFirstPlanPayment() && context.getCredit().isFirstPaymentOnlyPercent()) {
                subtractPeriodCount = 0;
            } else if (advancePaymentsWithDelayedRecalculationExist()) {
                BigDecimal advancePaymentTotalSum = Stream.of(advancePaymentEventsWithDelayedRecalculation).map(x -> x.getPayment().getSum()).reduce(BigDecimal.ZERO, (x, y) -> x.add(y));
                BigDecimal difference = context.getCurrentPlanPayment().getSum().subtract(advancePaymentTotalSum);
                if (difference.compareTo(BigDecimal.ZERO) > 0) {
                    paymentSum = difference;
                }
            }
        } else if (paymentSum.compareTo(percentSum) <= 0) {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
            throw new CreditCalculationException(String.format("Сумма планового платежа %.2f меньше или равна сумме процентов %.2f, платёж от %s",
                    paymentSum, percentSum, dateFormatter.format(getDate().getTime())));
        }

        BigDecimal mainDebtSum = paymentSum.subtract(percentSum);
        context.decreaseRemainMainDebtSum(mainDebtSum);
        context.setRemainPeriodInMonth(context.getRemainPeriodInMonth() - subtractPeriodCount);

        BigDecimal remainMainDebtSum = context.getRemainMainDebtSum();
        if (remainMainDebtSum.compareTo(BigDecimal.ZERO) < 0) {
            paymentSum = paymentSum.add(remainMainDebtSum);
            mainDebtSum = mainDebtSum.add(remainMainDebtSum);
            remainMainDebtSum = BigDecimal.ZERO;
        }

        if (advancePaymentsWithDelayedRecalculationExist()) {
            Stream.of(advancePaymentEventsWithDelayedRecalculation).forEach(e -> e.handleAdvancePaymentByType(context));
        }

        return new PaymentGraphEvent(context.getCurrentPeriodNumber(), getDate(), paymentSum,
                percentSum, mainDebtSum, remainMainDebtSum, eventType);
    }

    private boolean advancePaymentsWithDelayedRecalculationExist() {
        return advancePaymentEventsWithDelayedRecalculation != null && advancePaymentEventsWithDelayedRecalculation.size() > 0;
    }

    public Calendar getStartPercentDate() {
        return startPercentDate;
    }

    private void setStartPercentDate(@NonNull Calendar startPercentDate) {
        if (startPercentDate == null) {
            throw new IllegalArgumentException("Начальная дата, от которой необходимо рассчитывать проценты, не может быть равна null");
        }
        this.startPercentDate = startPercentDate;
    }

    public boolean isOnlyPercent() {
        return onlyPercent;
    }

    public void setOnlyPercent(boolean onlyPercent) {
        this.onlyPercent = onlyPercent;
    }

    public List<AdvancePaymentEvent> getAdvancePaymentEventsWithDelayedRecalculation() {
        return advancePaymentEventsWithDelayedRecalculation;
    }

    public void setAdvancePaymentEventsWithDelayedRecalculation(List<AdvancePaymentEvent> advancePaymentEventsWithDelayedRecalculation) {
        this.advancePaymentEventsWithDelayedRecalculation = advancePaymentEventsWithDelayedRecalculation;
    }
}
