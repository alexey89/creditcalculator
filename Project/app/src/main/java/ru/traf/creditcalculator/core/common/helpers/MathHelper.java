package ru.traf.creditcalculator.core.common.helpers;

/**
 * Вспомогательный класс для математических операций
 */
public class MathHelper {
    private MathHelper() {
    }

    /**
     * Округление до нужного кол-ва знаков после запятой
     * @param value Число, которое нужно округлить
     * @param decimals Кол-во знаков после запятой
     */
    public static double round(double value, int decimals) {
        if (decimals < 0) {
            throw new IllegalArgumentException("Кол-во знаков после запятой должно быть больше или равно нулю");
        }

        double factor = Math.pow(10, decimals);
        return (double) Math.round(value * factor) / factor;
    }

    /**
     * Вычисляет логарифм по произвольному основанию
     * @param value Число, от которого нужно вычислить логарфим
     * @param base Основание
     */
    public static double logOfBase(double value, double base) {
        return Math.log(value) / Math.log(base);
    }
}
