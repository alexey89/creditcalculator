package ru.traf.creditcalculator.core.common.exceptions;

/**
 * Исключение при расчёте графика платежей по кредиту
 */
public class CreditCalculationException extends Exception {
    public CreditCalculationException(String detailMessage) {
        super(detailMessage);
    }
}
