package ru.traf.creditcalculator.core.common.helpers;

import java.util.Calendar;

/**
 * Вспомогательнй класс для работы с календарём
 */
public final class CalendarHelper {
    private CalendarHelper() {
    }

    /**
     * Возвращает разницу между двумя датами в днях
     */
    public static int getDateDifferenceInDays(Calendar startDate, Calendar endDate) {
        if (startDate == null) {
            throw new IllegalArgumentException("Начальная дата не может быть равна null");
        }
        if (endDate == null) {
            throw new IllegalArgumentException("Конечная дата не может быть равна null");
        }

        long differenceInMillis = endDate.getTimeInMillis() - startDate.getTimeInMillis();
        return (int) (differenceInMillis / (1000 * 60 * 60 * 24));
    }

    /**
     * Устанавливает переданную дату в последний день месяца
     */
    public static void setLastDayOfMonth(Calendar date) {
        if (date == null) {
            throw new IllegalArgumentException("Дата не может быть равна null");
        }

        int lastDayOfMonth = date.getActualMaximum(Calendar.DAY_OF_MONTH);
        date.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
    }
}