package ru.traf.creditcalculator.core.creditevents;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

import ru.traf.creditcalculator.core.AdvancePayment;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.periodcalculators.IPeriodCalculator;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;

/**
 * Событие "Досрочный платёж в дату планового платежа"
 */
public class AdvancePaymentOnPlanPaymentDateEvent extends AdvancePaymentEvent {
    public AdvancePaymentOnPlanPaymentDateEvent(@NonNull Calendar date, @NonNull AdvancePayment payment,
                                                IPeriodCalculator periodCalculator, IPlanPaymentCalculator planPaymentCalculator) {
        super(date, payment, periodCalculator, planPaymentCalculator);
    }

    @Override
    public final PaymentGraphEvent handle(ICreditContext context) throws CreditCalculationException {
        BigDecimal advancePaymentSum = payment.getSum();
        if (payment.isSubtractPlanPaymentSum()) {
            advancePaymentSum = advancePaymentSum.subtract(context.getCurrentPlanPayment().getSum());
            if (advancePaymentSum.compareTo(BigDecimal.ZERO) <= 0) {
                throw new CreditCalculationException(String.format("Сумма досрочного платежа %.2f меньше или равна нулю после вычета планового платежа, равного %.2f",
                        payment.getSum(), context.getCurrentPlanPayment().getSum()));
            }
        }
        context.decreaseRemainMainDebtSum(advancePaymentSum);
        handleAdvancePaymentByType(context);
        return new PaymentGraphEvent(context.getCurrentPeriodNumber(), getDate(), advancePaymentSum,
                BigDecimal.ZERO, advancePaymentSum, context.getRemainMainDebtSum(), eventType);
    }
}
