package ru.traf.creditcalculator.core;

/**
 * Тип кредитного события
 */
public enum CreditEventType {
    PlanPayment("Плановый платёж"),
    AdvancePayment("Досрочный платёж"),
    PercentRateChange("Изменение процентной ставки"),
    PlanPaymentChange("Изменение планового платежа");

    private String description;

    CreditEventType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}