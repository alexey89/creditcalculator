package ru.traf.creditcalculator.core.common;

/**
 * Период для специальных дней
 */
public enum SpecialDayPeriod {
    /**
     * Неделя
     */
    Week,

    /**
     * Год
     */
    Year,

    /**
     * Без периода
     */
    WithoutPeriod,
}
