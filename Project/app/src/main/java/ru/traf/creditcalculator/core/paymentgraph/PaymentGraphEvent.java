package ru.traf.creditcalculator.core.paymentgraph;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

import ru.traf.creditcalculator.core.CreditEventType;

/**
 * Событие графика платежей
 */
public class PaymentGraphEvent {
    /**
     * Номер текущего платёжного периода
     */
    private int paymentPeriodNumber;

    /**
     * Дата события
     */
    private Calendar date;

    /**
     * Сумма платежа
     */
    private BigDecimal paymentSum;

    /**
     * Сумма в уплату процентов
     */
    private BigDecimal percentSum;

    /**
     * Сумма в уплату основного долга
     */
    private BigDecimal mainDebtSum;

    /**
     * Остаток основного долга
     */
    private BigDecimal remainMainDebtSum;

    /**
     * Тип события
     */
    private CreditEventType eventType;

    /**
     * Описание события
     */
    private String description;

    public PaymentGraphEvent(int paymentPeriodNumber, @NonNull Calendar date, @NonNull CreditEventType eventType) {
        this(paymentPeriodNumber, date, null, null, null, null, eventType, null);
    }

    public PaymentGraphEvent(int paymentPeriodNumber, @NonNull Calendar date, @NonNull CreditEventType eventType, String description) {
        this(paymentPeriodNumber, date, null, null, null, null, eventType, description);
    }

    public PaymentGraphEvent(int paymentPeriodNumber, @NonNull Calendar date, BigDecimal paymentSum, BigDecimal percentSum,
                             BigDecimal mainDebtSum, BigDecimal remainMainDebtSum, @NonNull CreditEventType eventType) {
        this(paymentPeriodNumber, date, paymentSum, percentSum, mainDebtSum, remainMainDebtSum, eventType, null);
    }

    public PaymentGraphEvent(int paymentPeriodNumber, @NonNull Calendar date, BigDecimal paymentSum, BigDecimal percentSum,
                             BigDecimal mainDebtSum, BigDecimal remainMainDebtSum, @NonNull CreditEventType eventType, String description) {
        setPaymentPeriodNumber(paymentPeriodNumber);
        setDate(date);
        setPaymentSum(paymentSum);
        setPercentSum(percentSum);
        setMainDebtSum(mainDebtSum);
        setRemainMainDebtSum(remainMainDebtSum);
        setEventType(eventType);
        setDescription(description);
    }

    public int getPaymentPeriodNumber() {
        return paymentPeriodNumber;
    }

    public void setPaymentPeriodNumber(int paymentPeriodNumber) {
        this.paymentPeriodNumber = paymentPeriodNumber;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(@NonNull Calendar date) {
        if (date == null) {
            throw new IllegalArgumentException("Дата события в графике платежей не может быть равна null");
        }
        this.date = date;
    }

    public BigDecimal getPaymentSum() {
        return paymentSum;
    }

    public void setPaymentSum(BigDecimal paymentSum) {
        this.paymentSum = paymentSum;
    }

    public BigDecimal getPercentSum() {
        return percentSum;
    }

    public void setPercentSum(BigDecimal percentSum) {
        this.percentSum = percentSum;
    }

    public BigDecimal getMainDebtSum() {
        return mainDebtSum;
    }

    public void setMainDebtSum(BigDecimal mainDebtSum) {
        this.mainDebtSum = mainDebtSum;
    }

    public BigDecimal getRemainMainDebtSum() {
        return remainMainDebtSum;
    }

    public void setRemainMainDebtSum(BigDecimal remainMainDebtSum) {
        this.remainMainDebtSum = remainMainDebtSum;
    }

    public CreditEventType getEventType() {
        return eventType;
    }

    public void setEventType(@NonNull CreditEventType eventType) {
        if (eventType == null) {
            throw new IllegalArgumentException("Тип события в графике платежей не может быть равен null");
        }
        this.eventType = eventType;
    }

    public String getDescription() {
        if (description == null || description.trim().length() == 0) {
            return eventType.getDescription();
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
