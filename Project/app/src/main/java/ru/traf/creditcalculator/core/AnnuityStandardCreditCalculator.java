package ru.traf.creditcalculator.core;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.creditevents.AdvancePaymentEvent;
import ru.traf.creditcalculator.core.creditevents.AdvancePaymentOnPlanPaymentDateEvent;
import ru.traf.creditcalculator.core.creditevents.CreditEvent;
import ru.traf.creditcalculator.core.creditevents.PercentRateChangeEvent;
import ru.traf.creditcalculator.core.creditevents.PlanPaymentChangeEvent;
import ru.traf.creditcalculator.core.creditevents.PlanPaymentEvent;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraph;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.percentsumcalculators.IPercentSumCalculator;
import ru.traf.creditcalculator.core.periodcalculators.IPeriodCalculator;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;

/**
 * Стандартный калькулятор для расчёта кредита с аннуитетными платежами
 */
public class AnnuityStandardCreditCalculator implements ICreditCalculator {
    /**
     * Целевой кредит
     */
    private Credit credit;

    /**
     * Текущий контекст кредита
     */
    private ICreditContext currentContext;

    /**
     * Расчётчик процентов
     */
    private IPercentSumCalculator percentSumCalculator;

    /**
     * График платежей
     */
    private PaymentGraph paymentGraph;

    /**
     * Расчётчик планового платежа
     */
    private IPlanPaymentCalculator planPaymentCalculator;

    /**
     * Расчётчик периода кредита
     */
    private IPeriodCalculator periodCalculator;

    public AnnuityStandardCreditCalculator(Credit credit, IPercentSumCalculator percentSumCalculator, IPlanPaymentCalculator planPaymentCalculator,
                                           IPeriodCalculator periodCalculator) {
        setCredit(credit);
        this.planPaymentCalculator = planPaymentCalculator;
        this.periodCalculator = periodCalculator;
        this.percentSumCalculator = percentSumCalculator;
    }

    /**
     * Инициализировать кредитный калькулятор
     */
    private void initCreditCalculator() {
        currentContext = new CreditContext(credit);
        paymentGraph = new PaymentGraph();
    }

    @Override
    public PaymentGraph calculate() throws CreditCalculationException {
        initCreditCalculator();

        outer:
        while (remainMainDebtGreaterThanZero()) {
            currentContext.goToNextPaymentPeriod();
            Iterable<CreditEvent> events = getCreditEventsForCurrentPaymentPeriod();
            for (CreditEvent event : events) {
                paymentGraph.AddPaymentGraphEvent(event.handle(currentContext));
                if (!remainMainDebtGreaterThanZero()) {
                    break outer;
                }
            }
        }
        return paymentGraph;
    }

    private boolean remainMainDebtGreaterThanZero() {
        return currentContext.getRemainMainDebtSum().compareTo(BigDecimal.ZERO) > 0;
    }

    private Iterable<CreditEvent> getCreditEventsForCurrentPaymentPeriod() {
        List<CreditEvent> events = new ArrayList<>(4);
        boolean onlyPercent = false;
        Calendar paymentStartPercentDate = currentContext.getPreviousPlanPaymentDate();
        Calendar nextPlanPaymentDate = currentContext.getNextPlanPaymentDate();

        Iterable<PlanPayment> planPaymentChanges = currentContext.getCurrentPaymentPeriodPlanPaymentChanges();
        Stream.of(planPaymentChanges).forEach(p -> events.add(new PlanPaymentChangeEvent(p.getStartDate(), p)));

        Iterable<AdvancePayment> advancePayments = currentContext.getCurrentPaymentPeriodAdvancePayments();
        List<AdvancePaymentEvent> advancePaymentEventsWithDelayedRecalculation = new ArrayList<>();
        for (AdvancePayment payment : advancePayments) {
            AdvancePaymentEvent event;
            if (payment.isPayOnPlanPaymentDate() || payment.getDate().compareTo(nextPlanPaymentDate) == 0) {
                event = new AdvancePaymentOnPlanPaymentDateEvent(currentContext.getNextPlanPaymentDate(), payment, periodCalculator, planPaymentCalculator);
            } else {
                event = new AdvancePaymentEvent(payment.getDate(), payment, paymentStartPercentDate, percentSumCalculator, periodCalculator, planPaymentCalculator);
                paymentStartPercentDate = payment.getDate();
                onlyPercent = payment.isOnlyPercentForNextPlanPayment();
                if (onlyPercent) {
                    event.setIsDelayedRecalculation(true);
                    advancePaymentEventsWithDelayedRecalculation.add(event);
                }
            }
            events.add(event);
        }

        Iterable<Rate> rateChanges = currentContext.getCurrentPaymentPeriodRateChanges();
        for (Rate rate : rateChanges) {
            CreditEvent event = new PercentRateChangeEvent(rate.getStartDate(), rate, planPaymentCalculator);
            if (rate.isApplyRateBeforePayment()) {
                event.setPriority((byte) 0);
            }
            events.add(event);
        }

        if (currentContext.isFirstPlanPayment() && credit.isFirstPaymentOnlyPercent()) {
            onlyPercent = true;
        }

        PlanPaymentEvent planPaymentEvent = new PlanPaymentEvent(currentContext.getNextPlanPaymentDate(), paymentStartPercentDate, onlyPercent, percentSumCalculator);
        planPaymentEvent.setAdvancePaymentEventsWithDelayedRecalculation(advancePaymentEventsWithDelayedRecalculation);
        events.add(planPaymentEvent);
        return Stream.of(events).sorted().collect(Collectors.toList());
    }

    public void setCredit(Credit credit) {
        if (credit == null) {
            throw new IllegalArgumentException("Кредит не может быть равен null");
        }
        if (credit.getType() == null || credit.getType() != CreditType.Annuity) {
            throw new IllegalArgumentException("Для аннуитетного калькулятора тип кредита должен быть равен \"Annuity\"");
        }
        this.credit = credit;
    }
}
