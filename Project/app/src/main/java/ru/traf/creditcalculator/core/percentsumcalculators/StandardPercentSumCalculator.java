package ru.traf.creditcalculator.core.percentsumcalculators;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.traf.creditcalculator.core.common.helpers.CalendarHelper;

/**
 * Стандартный расчётчик процентов
 */
public class StandardPercentSumCalculator implements IPercentSumCalculator {
    private BigDecimal remainMainDebtSum;
    private BigDecimal yearRate;

    @Override
    public BigDecimal calculate(Calendar startDate, Calendar endDate, BigDecimal remainMainDebtSum, BigDecimal yearRate) {
        this.remainMainDebtSum = remainMainDebtSum;
        this.yearRate = yearRate;
        Calendar localStartDate = startDate;
        Calendar localEndDate = endDate;
        BigDecimal percentSum = BigDecimal.ZERO;
        boolean differentYears = false;

        while (localEndDate.get(Calendar.YEAR) != localStartDate.get(Calendar.YEAR)) {
            int localStartDateYear = localStartDate.get(Calendar.YEAR);
            localEndDate = new GregorianCalendar(localStartDateYear, Calendar.DECEMBER, 31);
            percentSum = percentSum.add(calculateSum(CalendarHelper.getDateDifferenceInDays(localStartDate, localEndDate) + (differentYears ? 1 : 0),
                localStartDate.getActualMaximum(Calendar.DAY_OF_YEAR)));
            localStartDate = new GregorianCalendar(localStartDateYear + 1, Calendar.JANUARY, 1);
            localEndDate = endDate;
            differentYears = true;
        }

        percentSum = percentSum.add(calculateSum(CalendarHelper.getDateDifferenceInDays(localStartDate, localEndDate) + (differentYears ? 1 : 0),
                localStartDate.getActualMaximum(Calendar.DAY_OF_YEAR)));
        return percentSum.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    private BigDecimal calculateSum(int dateDifferenceInDays, int daysOfYearCount) {
        return remainMainDebtSum.multiply(yearRate).multiply(BigDecimal.valueOf((double) dateDifferenceInDays / daysOfYearCount));
    }
}
