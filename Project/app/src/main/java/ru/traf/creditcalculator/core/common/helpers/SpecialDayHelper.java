package ru.traf.creditcalculator.core.common.helpers;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import ru.traf.creditcalculator.core.common.SpecialDay;

/**
 * Вспомогательный класс для работы со специальными днями
 */
public final class SpecialDayHelper {
    private SpecialDayHelper() {
    }

    /**
     * Проверяет попадает ли дата в список переданных специальных дней
     */
    public static boolean isSpecialDay(@NonNull Calendar date, @NonNull Iterable<SpecialDay> specialDays) {
        if (date == null) {
            throw new IllegalArgumentException("Параметр \"date\" не может быть равен null");
        }
        if (specialDays == null) {
            throw new IllegalArgumentException("Параметр \"specialDays\" не может быть равен null");
        }

        for (SpecialDay day : specialDays) {
            switch (day.getPeriod()) {
                case Week:
                    if (date.get(Calendar.DAY_OF_WEEK) == day.getDayOfWeek()) {
                        return true;
                    }
                    break;
                case Year:
                    if (date.get(Calendar.MONTH) == day.getMonth() && date.get(Calendar.DAY_OF_MONTH) == day.getDayOfMonth()) {
                        return true;
                    }
                    break;
                case WithoutPeriod:
                    if (date.equals(day.getDate())) {
                        return true;
                    }
                    break;
                default:
                    throw new IllegalStateException("Неизвестный тип периода: " + day.getPeriod());
            }
        }
        return false;
    }

    /**
     * Возвращает стандартный список выходных дней
     */
    public static Collection<SpecialDay> getStandardDaysOff() {
        ArrayList<SpecialDay> daysOff = new ArrayList<>();
        daysOff.add(new SpecialDay((byte) Calendar.SUNDAY));
        daysOff.add(new SpecialDay((byte) Calendar.JANUARY, (byte) 1));
        daysOff.add(new SpecialDay((byte) Calendar.JANUARY, (byte) 2));
        daysOff.add(new SpecialDay((byte) Calendar.FEBRUARY, (byte) 23));
        daysOff.add(new SpecialDay((byte) Calendar.MARCH, (byte) 8));
        daysOff.add(new SpecialDay((byte) Calendar.MAY, (byte) 1));
        daysOff.add(new SpecialDay((byte) Calendar.MAY, (byte) 9));
        daysOff.add(new SpecialDay((byte) Calendar.JUNE, (byte) 12));
        daysOff.add(new SpecialDay((byte) Calendar.NOVEMBER, (byte) 4));
        return daysOff;
    }

    /**
     * Переносит дату на первый рабочий день, если она приходится на выходной
     */
    public static Calendar transferDayOff(Calendar date, Iterable<SpecialDay> daysOff, Iterable<SpecialDay> workDays) {
        while (SpecialDayHelper.isSpecialDay(date, daysOff)) {
            if (SpecialDayHelper.isSpecialDay(date, workDays)) {
                break;
            }
            date.add(Calendar.DAY_OF_MONTH, 1);
        }
        return date;
    }
}
