package ru.traf.creditcalculator.core.planpaymentcalculators;

import java.math.BigDecimal;

/**
 * Интерфейс расчётчика планового платежа
 */
public interface IPlanPaymentCalculator {
    BigDecimal calculate(BigDecimal creditSum, int creditPeriodInMouth, double monthRate);
}
