package ru.traf.creditcalculator.core;

/**
 * Тип досрочного платежа
 */
public enum AdvancePaymentType {
    /**
     * В уменьшение суммы платежа
     */
    PaymentDecreasing,

    /**
     * В уменьшения срока кредита
     */
    PeriodDecreasing,
}
