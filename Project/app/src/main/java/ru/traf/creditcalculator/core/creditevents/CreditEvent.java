package ru.traf.creditcalculator.core.creditevents;

import java.util.Calendar;

import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;

/**
 * Событие кредита
 */
public abstract class CreditEvent implements Comparable<CreditEvent> {
    /**
     * Дата события
     */
    private Calendar date;

    /**
     * Приоритет события
     */
    private byte priority;

    protected CreditEvent(Calendar date, byte priority) {
        setDate(date);
        setPriority(priority);
    }

    /**
     * Обработать событие
     */
    public abstract PaymentGraphEvent handle(ICreditContext context) throws CreditCalculationException;

    @Override
    public int compareTo(CreditEvent another) {
        int result = date.compareTo(another.date);
        if (result == 0) {
            return priority < another.priority ? -1 : (priority == another.priority ? 0 : 1);
        }
        return result;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        if (date == null) {
            throw new IllegalArgumentException("Дата события не может быть равна null");
        }
        this.date = date;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }
}
