package ru.traf.creditcalculator.core;

/**
 * Тип кредита
 */
public enum CreditType {
    /**
     * Аннуитетный
     */
    Annuity,

    /**
     * Дифференцированный
     */
    Differential,
}
