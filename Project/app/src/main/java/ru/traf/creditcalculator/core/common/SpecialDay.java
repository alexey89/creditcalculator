package ru.traf.creditcalculator.core.common;

import android.support.annotation.NonNull;

import java.util.Calendar;

/**
 * Специальные дни
 */
public class SpecialDay {
    /**
     * День недели
     */
    private byte dayOfWeek;

    /**
     * Месяц
     */
    private byte month;

    /**
     * День месяца
     */
    private byte dayOfMonth;

    /**
     * Дата
     */
    private Calendar date;

    /**
     * Период
     */
    private SpecialDayPeriod period;

    /**
     * Создаёт специальный день с периодом Week
     * @param dayOfWeek День недели от 1 до 7 (воскресенье - 1, суббота - 7)
     */
    public SpecialDay(byte dayOfWeek) {
        if (dayOfWeek < 1 || dayOfWeek > 7) {
            throw new IllegalArgumentException("День недели должен находиться в диапазоне от 1 до 7 включительно");
        }
        this.dayOfWeek = dayOfWeek;
        this.period = SpecialDayPeriod.Week;
    }

    /**
     * Создаёт специальный день с периодом Year
     * @param month      Месяц (от 0 до 11)
     * @param dayOfMonth День месяца (от 1 до 31)
     */
    public SpecialDay(byte month, byte dayOfMonth) {
        if (month < 0 || month > 11) {
            throw new IllegalArgumentException("Месяц должен находиться в диапазоне от 0 до 11 включительно");
        }
        if (dayOfMonth < 1 || dayOfMonth > 31) {
            throw new IllegalArgumentException("День месяца должен находиться в диапазоне от 1 до 31 включительно");
        }
        this.month = month;
        this.dayOfMonth = dayOfMonth;
        this.period = SpecialDayPeriod.Year;
    }

    /**
     * Создаёт специальный день с конкретной датой без периода
     * @param date Конкретная дата
     */
    public SpecialDay(@NonNull Calendar date) {
        if (date == null) {
            throw new IllegalArgumentException("Дата не может быть равна нулю");
        }
        this.date = date;
        this.period = SpecialDayPeriod.WithoutPeriod;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public byte getMonth() {
        return month;
    }

    public byte getDayOfMonth() {
        return dayOfMonth;
    }

    public Calendar getDate() {
        return date;
    }

    public SpecialDayPeriod getPeriod() {
        return period;
    }
}
