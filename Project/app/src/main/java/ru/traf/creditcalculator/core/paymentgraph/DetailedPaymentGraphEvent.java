package ru.traf.creditcalculator.core.paymentgraph;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

import ru.traf.creditcalculator.core.CreditEventType;

/**
 * Детализированное событие графика платежей
 */
@Deprecated
public class DetailedPaymentGraphEvent extends PaymentGraphEvent {
    /**
     * Сумма платежа
     */
    private BigDecimal paymentSum;

    /**
     * Сумма в уплату процентов
     */
    private BigDecimal percentSum;

    /**
     * Сумма в уплату основного долга
     */
    private BigDecimal mainDebtSum;

    /**
     * Остаток основного долга
     */
    private BigDecimal remainMainDebtSum;

    public DetailedPaymentGraphEvent(int paymentPeriodNumber, @NonNull Calendar date, @NonNull BigDecimal paymentSum, @NonNull BigDecimal percentSum,
                                     @NonNull BigDecimal mainDebtSum, @NonNull BigDecimal remainMainDebtSum, @NonNull CreditEventType eventType, String description) {
        super(paymentPeriodNumber, date, eventType, description);
        setPaymentSum(paymentSum);
        setPercentSum(percentSum);
        setMainDebtSum(mainDebtSum);
        setRemainMainDebtSum(remainMainDebtSum);
    }

    public BigDecimal getPaymentSum() {
        return paymentSum;
    }

    public void setPaymentSum(@NonNull BigDecimal paymentSum) {
        if (paymentSum == null) {
            throw new IllegalArgumentException("Сумма платежа не может быть равна null");
        }
        this.paymentSum = paymentSum;
    }

    public BigDecimal getPercentSum() {
        return percentSum;
    }

    public void setPercentSum(@NonNull BigDecimal percentSum) {
        if (percentSum == null) {
            throw new IllegalArgumentException("Сумма в уплату процентов не может быть равна null");
        }
        this.percentSum = percentSum;
    }

    public BigDecimal getMainDebtSum() {
        return mainDebtSum;
    }

    public void setMainDebtSum(@NonNull BigDecimal mainDebtSum) {
        if (mainDebtSum == null) {
            throw new IllegalArgumentException("Сумма в уплату основного долга не может быть равна null");
        }
        this.mainDebtSum = mainDebtSum;
    }

    public BigDecimal getRemainMainDebtSum() {
        return remainMainDebtSum;
    }

    public void setRemainMainDebtSum(@NonNull BigDecimal remainMainDebtSum) {
        if (remainMainDebtSum == null) {
            throw new IllegalArgumentException("Остаток основного долга не может быть равен null");
        }
        this.remainMainDebtSum = remainMainDebtSum;
    }
}
