package ru.traf.creditcalculator.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.PriorityQueue;
import java.util.Queue;

import ru.traf.creditcalculator.core.common.helpers.CalendarHelper;
import ru.traf.creditcalculator.core.common.helpers.SpecialDayHelper;

/**
 * Контекст кредита
 */
public class CreditContext implements ICreditContext {
    /**
     * Очередь плановых платежей, упорядоченная по дате
     */
    private Queue<PlanPayment> planPayments = new PriorityQueue<>(1, (p1, p2) -> p1.getStartDate().compareTo(p2.getStartDate()));

    /**
     * Очередь процентных ставок, упорядоченная по дате
     */
    private Queue<Rate> rates = new PriorityQueue<>(1, (r1, r2) -> r1.getStartDate().compareTo(r2.getStartDate()));

    /**
     * Очередь досрочных платежей, упорядоченная по дате
     */
    private Queue<AdvancePayment> advancePayments = new PriorityQueue<>(16, (p1, p2) -> p1.getDate().compareTo(p2.getDate()));

    /**
     * Оставшаяся сумма основного долга
     */
    private BigDecimal remainMainDebtSum;

    /**
     * Оставшийся период кредита в месяцах
     */
    private int remainPeriodInMonth;

    /**
     * Текущий плановый платёж
     */
    private PlanPayment currentPlanPayment;

    /**
     * Текущая процентная ставка
     */
    private Rate currentRate;

    /**
     * Дата предыдущего планового платежа
     */
    private Calendar previousPlanPaymentDate;

    /**
     * Точная дата предыдущего планового платежа (без учёта переноса выходных дней)
     */
    private Calendar exactNextPlanPaymentDate;

    /**
     * Дата следующего планового платежа
     */
    private Calendar nextPlanPaymentDate;

    /**
     * Текущий номер платёжного периода
     */
    private int currentPeriodNumber;

    /**
     * Изменения плановых платежей в текущем платёжном периоде
     */
    private Iterable<PlanPayment> currentPaymentPeriodPlanPaymentChanges;

    /**
     * Изменения процентных ставок в текущем платёжном периоде
     */
    private Iterable<Rate> currentPaymentPeriodRateChanges;

    /**
     * Досрочные платежи для текущего платёжного периода
     */
    private Iterable<AdvancePayment> currentPaymentPeriodAdvancePayments;

    /**
     * Изменялась ли сумма текущего платежа
     */
    private boolean isPlanPaymentSumChanged;

    /**
     * Исходный кредит
     */
    private Credit credit;

    public CreditContext(Credit credit) {
        this.credit = credit;
        planPayments.addAll(credit.getPlanPayments());
        rates.addAll(credit.getRates());
        if (credit.getAdvancePayments() != null) {
            advancePayments.addAll(credit.getAdvancePayments());
        }
        remainMainDebtSum = credit.getSum();
        remainPeriodInMonth = credit.getPeriodInMonth();
        currentPlanPayment = planPayments.poll();
        currentRate = rates.poll();
        currentPeriodNumber = 0;
    }

    @Override
    public void goToNextPaymentPeriod() {
        currentPeriodNumber++;
        if (currentPeriodNumber == 1) {
            previousPlanPaymentDate = credit.isFirstPaymentOnlyPercent()
                    ? credit.getCreditDate()
                    : getFictitiousPreviousPlanPaymentDateForFirstPayment();
            exactNextPlanPaymentDate = (Calendar) currentPlanPayment.getStartDate().clone();
            removeInvalidAdvancePaymentsIfNeed();
        } else {
            previousPlanPaymentDate = nextPlanPaymentDate;
            exactNextPlanPaymentDate.add(Calendar.MONTH, 1);
        }

        setLastDayOfMonthIfNeed(exactNextPlanPaymentDate);
        nextPlanPaymentDate = (Calendar) exactNextPlanPaymentDate.clone();
        if (currentPlanPayment.isTransferDaysOff()) {
            SpecialDayHelper.transferDayOff(nextPlanPaymentDate, credit.getDaysOff(), credit.getWorkDays());
        }

        currentPaymentPeriodPlanPaymentChanges = pollPlanPaymentsWithLessOrEqualDateFromQueue(nextPlanPaymentDate);
        currentPaymentPeriodRateChanges = pollRatesWithLessOrEqualDateFromQueue(nextPlanPaymentDate);
        currentPaymentPeriodAdvancePayments = pollAdvancePaymentsWithLessOrEqualDateFromQueue(nextPlanPaymentDate);
    }

    /**
     * Получить фиктивную предыдущую дату планового платежа для первого планового платежа
     */
    private Calendar getFictitiousPreviousPlanPaymentDateForFirstPayment() {
        Calendar fictitiousPreviousDate = (Calendar) currentPlanPayment.getStartDate().clone();
        fictitiousPreviousDate.add(Calendar.MONTH, -1);
        setLastDayOfMonthIfNeed(fictitiousPreviousDate);
        return fictitiousPreviousDate;
    }

    /**
     * Устанавливает дату на последний день месяца при необходимости
     */
    private void setLastDayOfMonthIfNeed(Calendar date) {
        if (currentPlanPayment.isLastDayOfMonth()) {
            CalendarHelper.setLastDayOfMonth(date);
        }
    }

    /**
     * Удалить некорреткные досрочные платежи из очереди, если таковые имеются
     */
    private void removeInvalidAdvancePaymentsIfNeed() {
        pollAdvancePaymentsWithLessDateFromQueue(previousPlanPaymentDate);
    }

    /**
     * Получить из очереди досрочные платежи с датой, меньшей переданной
     */
    private Iterable<AdvancePayment> pollAdvancePaymentsWithLessDateFromQueue(Calendar date) {
        return pollAdvancePaymentsFromQueue(date, true);
    }

    /**
     * Получить из очереди досрочные платежи с датой, меньшей или равной переданной
     */
    private Iterable<AdvancePayment> pollAdvancePaymentsWithLessOrEqualDateFromQueue(Calendar date) {
        return pollAdvancePaymentsFromQueue(date, false);
    }

    /**
     * Получить из очереди досрочные платежи, исходя из переданной даты
     * @param onlyWithLessDate True - только платежи с датой, меньшей переданной; false - платежи с датой, меньшей или равной переданной
     */
    private Iterable<AdvancePayment> pollAdvancePaymentsFromQueue(Calendar date, boolean onlyWithLessDate) {
        ArrayList<AdvancePayment> result = new ArrayList<>();
        while (true) {
            AdvancePayment payment = advancePayments.peek();
            if (payment != null && payment.getDate().compareTo(date) <= (onlyWithLessDate ? -1 : 0)) {
                result.add(payment);
                advancePayments.poll();
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * Получить из очереди плановые платежи с датой, меньшей или равной переданной
     */
    private Iterable<PlanPayment> pollPlanPaymentsWithLessOrEqualDateFromQueue(Calendar date) {
        ArrayList<PlanPayment> result = new ArrayList<>();
        while (true) {
            PlanPayment payment = planPayments.peek();
            if (payment != null && payment.getStartDate().compareTo(date) <= 0) {
                result.add(payment);
                planPayments.poll();
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * Получить из очереди процентные ставки с датой, меньшей или равной переданной
     */
    private Iterable<Rate> pollRatesWithLessOrEqualDateFromQueue(Calendar date) {
        ArrayList<Rate> result = new ArrayList<>();
        while (true) {
            Rate rate = rates.peek();
            if (rate != null && rate.getStartDate().compareTo(date) <= 0) {
                result.add(rate);
                rates.poll();
            } else {
                break;
            }
        }
        return result;
    }

    @Override
    public Iterable<PlanPayment> getCurrentPaymentPeriodPlanPaymentChanges() {
        return currentPaymentPeriodPlanPaymentChanges;
    }

    @Override
    public Iterable<Rate> getCurrentPaymentPeriodRateChanges() {
        return currentPaymentPeriodRateChanges;
    }

    @Override
    public Iterable<AdvancePayment> getCurrentPaymentPeriodAdvancePayments() {
        return currentPaymentPeriodAdvancePayments;
    }

    @Override
    public BigDecimal getRemainMainDebtSum() {
        return remainMainDebtSum;
    }

    @Override
    public void decreaseRemainMainDebtSum(BigDecimal sum) {
        remainMainDebtSum = remainMainDebtSum.subtract(sum);
    }

    @Override
    public int getRemainPeriodInMonth() {
        return remainPeriodInMonth;
    }

    @Override
    public void setRemainPeriodInMonth(int newPeriod) {
        remainPeriodInMonth = newPeriod;
    }

    @Override
    public void setCurrentPlanPayment(PlanPayment payment) {
        currentPlanPayment = payment;
        isPlanPaymentSumChanged = false;
    }

    @Override
    public PlanPayment getCurrentPlanPayment() {
        return currentPlanPayment;
    }

    @Override
    public void changeCurrentPlanPaymentSum(BigDecimal newSum) {
        if (!isPlanPaymentSumChanged) {
            currentPlanPayment = currentPlanPayment.clone();
        }
        currentPlanPayment.setSum(newSum);
        isPlanPaymentSumChanged = true;
    }

    @Override
    public void setCurrentRate(Rate newRate) {
        currentRate = newRate;
    }

    @Override
    public Rate getCurrentRate() {
        return currentRate;
    }

    @Override
    public Calendar getNextPlanPaymentDate() {
        return nextPlanPaymentDate;
    }

    @Override
    public Calendar getPreviousPlanPaymentDate() {
        return previousPlanPaymentDate;
    }

    @Override
    public int getCurrentPeriodNumber() {
        return currentPeriodNumber;
    }

    @Override
    public boolean isFirstPlanPayment() {
        return currentPeriodNumber == 1;
    }

    @Override
    public Credit getCredit() {
        return credit;
    }
}
