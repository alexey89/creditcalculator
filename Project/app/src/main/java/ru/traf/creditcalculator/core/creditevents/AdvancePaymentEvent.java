package ru.traf.creditcalculator.core.creditevents;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

import ru.traf.creditcalculator.core.AdvancePayment;
import ru.traf.creditcalculator.core.CreditEventType;
import ru.traf.creditcalculator.core.ICreditContext;
import ru.traf.creditcalculator.core.common.exceptions.CreditCalculationException;
import ru.traf.creditcalculator.core.paymentgraph.PaymentGraphEvent;
import ru.traf.creditcalculator.core.percentsumcalculators.IPercentSumCalculator;
import ru.traf.creditcalculator.core.periodcalculators.IPeriodCalculator;
import ru.traf.creditcalculator.core.planpaymentcalculators.IPlanPaymentCalculator;

/**
 * Событие "Досрочный платёж в дату платежа"
 */
public class AdvancePaymentEvent extends CreditEvent {
    private static final byte DEFAULT_PRIORITY = 3;
    static CreditEventType eventType = CreditEventType.AdvancePayment;

    /**
     * Досрочный платёж
     */
    protected AdvancePayment payment;

    /**
     * Начальная дата, от которой рассчитывать проценты
     */
    private Calendar startPercentDate;

    /**
     * Флаг - является ли пересчёт срока или платежа отложенным
     */
    private boolean isDelayedRecalculation;

    /**
     * Сумма остатка основного долга, по которой необходимо производить отложенный пересчёт суммы или периода кредита
     */
    BigDecimal remainMainDebtSumForDelayedRecalculation;

    private IPercentSumCalculator percentSumCalculator;
    private IPeriodCalculator periodCalculator;
    private IPlanPaymentCalculator planPaymentCalculator;

    protected AdvancePaymentEvent(@NonNull Calendar date, @NonNull AdvancePayment payment,
                                  IPeriodCalculator periodCalculator, IPlanPaymentCalculator planPaymentCalculator) {
        this(date, payment, null, null, periodCalculator, planPaymentCalculator);
    }

    public AdvancePaymentEvent(@NonNull Calendar date, @NonNull AdvancePayment payment, Calendar startPercentDate,
                               IPercentSumCalculator percentSumCalculator, IPeriodCalculator periodCalculator,
                               IPlanPaymentCalculator planPaymentCalculator) {
        super(date, DEFAULT_PRIORITY);
        setPayment(payment);
        setStartPercentDate(startPercentDate);
        this.percentSumCalculator = percentSumCalculator;
        this.periodCalculator = periodCalculator;
        this.planPaymentCalculator = planPaymentCalculator;
    }

    @Override
    public PaymentGraphEvent handle(ICreditContext context) throws CreditCalculationException {
        BigDecimal percentSum = percentSumCalculator.calculate(startPercentDate, getDate(), context.getRemainMainDebtSum(), context.getCurrentRate().getYearRate());
        if (percentSum.compareTo(payment.getSum()) > 0) {
            throw new CreditCalculationException(String.format("Сумма досрочного платежа %.2f не покрывает проценты", payment.getSum()));
        }
        BigDecimal mainDebtSum = payment.getSum().subtract(percentSum);
        context.decreaseRemainMainDebtSum(mainDebtSum);
        if (!isDelayedRecalculation) {
            handleAdvancePaymentByType(context);
        } else {
            remainMainDebtSumForDelayedRecalculation = context.getRemainMainDebtSum();
        }
        return new PaymentGraphEvent(context.getCurrentPeriodNumber(), getDate(), payment.getSum(),
                percentSum, mainDebtSum, context.getRemainMainDebtSum(), eventType);
    }

    protected void handleAdvancePaymentByType(ICreditContext context) {
        BigDecimal remainMainDebtSum = isDelayedRecalculation ? remainMainDebtSumForDelayedRecalculation : context.getRemainMainDebtSum();
        switch (payment.getType()) {
            case PeriodDecreasing:
                int newRemainPeriodInMonth = periodCalculator.calculate(remainMainDebtSum,
                        context.getCurrentPlanPayment().getSum(), context.getCurrentRate().getMonthRate());
                if (newRemainPeriodInMonth < context.getRemainPeriodInMonth()) {
                    context.setRemainPeriodInMonth(newRemainPeriodInMonth);
                }
                break;
            case PaymentDecreasing:
                if (context.getRemainPeriodInMonth() <= 0) {
                    break;
                }

                BigDecimal newPlanPaymentSum = planPaymentCalculator.calculate(remainMainDebtSum,
                        context.getRemainPeriodInMonth(), context.getCurrentRate().getMonthRate());
                if (newPlanPaymentSum.compareTo(context.getCurrentPlanPayment().getSum()) >= 0) {
                    break;
                }

                if (newPlanPaymentSum.compareTo(BigDecimal.ZERO) < 0) {
                    newPlanPaymentSum = BigDecimal.ZERO;
                }
                context.changeCurrentPlanPaymentSum(newPlanPaymentSum);
                break;
            default:
                throw new IllegalStateException("Неизвестный тип досрочного платежа \"" + payment.getType() + "\"");
        }
    }

    public AdvancePayment getPayment() {
        return payment;
    }

    private void setPayment(@NonNull AdvancePayment payment) {
        if (payment == null) {
            throw new IllegalArgumentException("Досрочный платёж не может быть равен null");
        }
        this.payment = payment;
    }

    public Calendar getStartPercentDate() {
        return startPercentDate;
    }

    public void setStartPercentDate(Calendar startPercentDate) {
        this.startPercentDate = startPercentDate;
    }

    public boolean isDelayedRecalculation() {
        return isDelayedRecalculation;
    }

    public void setIsDelayedRecalculation(boolean isDelayedRecalculation) {
        this.isDelayedRecalculation = isDelayedRecalculation;
    }
}
