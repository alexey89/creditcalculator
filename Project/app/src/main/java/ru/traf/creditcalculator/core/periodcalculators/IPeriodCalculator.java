package ru.traf.creditcalculator.core.periodcalculators;

import java.math.BigDecimal;

/**
 * Расчётчик периода кредита
 */
public interface IPeriodCalculator {
    int calculate(BigDecimal remainDebtSum, BigDecimal planPaymentSum, double monthRate);
}
