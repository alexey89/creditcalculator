package ru.traf.creditcalculator.core;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Процентная ставка
 */
public class Rate {
    /**
     * Годовая ставка в процентах
     */
    private BigDecimal yearRateInPercent;

    /**
     * Дата вступления в силу
     */
    private Calendar startDate;

    /**
     * Применять процентную ставку перед любыми платежами при изменении
     */
    private boolean applyRateBeforePayment;

    /**
     * Перерасчитывать ли плановый платёж при изменении процентной ставки
     */
    private boolean isRecalculatePlanPayment = true;

    public Rate(BigDecimal yearRateInPercent, @NonNull Calendar startDate) {
        setYearRateInPercent(yearRateInPercent);
        setStartDate(startDate);
    }

    public BigDecimal getYearRate() {
        return yearRateInPercent.divide(BigDecimal.valueOf(100));
    }

    public BigDecimal getYearRateInPercent() {
        return yearRateInPercent;
    }

    public void setYearRateInPercent(BigDecimal yearRateInPercent) {
        if (yearRateInPercent.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Процентная ставка должна быть больше или равна нулю");
        }
        this.yearRateInPercent = yearRateInPercent;
    }

    public double getMonthRate() {
        return getYearRate().doubleValue() / 12;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(@NonNull Calendar startDate) {
        if (startDate == null) {
            throw new IllegalArgumentException("Дата вступления в силу процентной ставки не может быть null");
        }
        this.startDate = startDate;
    }

    public boolean isApplyRateBeforePayment() {
        return applyRateBeforePayment;
    }

    public void setApplyRateBeforePayment(boolean applyRateBeforePayment) {
        this.applyRateBeforePayment = applyRateBeforePayment;
    }

    public boolean isRecalculatePlanPayment() {
        return isRecalculatePlanPayment;
    }

    public void setRecalculatePlanPayment(boolean recalculatePlanPayment) {
        this.isRecalculatePlanPayment = recalculatePlanPayment;
    }
}
